package au.com.ourpillstalkpaid.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by luongvo on 8/1/18.
 */
@Table(name = "MonthNames")
public class MonthName extends Model {

    @Column(name = "name")
    private String name;

    /**
     * Empty constructor required by ActiveAndroid (when selecting data from SQLite)
     */
    public MonthName() {
    }

    public MonthName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
