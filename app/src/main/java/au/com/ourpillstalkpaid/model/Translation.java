package au.com.ourpillstalkpaid.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by luongvo on 7/24/18.
 */
@Root(strict = false)
@Table(name = "Translation")
public class Translation extends Model {

    @Element(name = "From")
    @Column(name = "fromColumn")
    private String from;
    @Element(name = "To")
    @Column(name = "toColumn")
    private String to;

    @Column(name = "isFull")
    private boolean isFull;

    /**
     * Empty constructor required by ActiveAndroid (when selecting data from SQLite)
     */
    public Translation() {
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public void setFull(boolean full) {
        isFull = full;
    }

    public static List<Translation> findTranslations(boolean isFull) {
        return new Select().from(Translation.class).where("isFull = ?", isFull).execute();
    }

    public static boolean fullTranslation(List<String> texts) {
        List<Translation> fullTranslations = Translation.findTranslations(true);
        for (Translation translation : fullTranslations) {
            boolean translated = false;
            for (int i = 0; i < texts.size(); i++) {
                if (translation.getFrom().equalsIgnoreCase(texts.get(i))) {
                    texts.set(i, translation.getTo());
                    translated = true;
                }
            }
            if (translated) {
                return true;
            }
        }
        return false;
    }

    public static void partialTranslation(List<String> texts) {
        List<Translation> partialTranslations = Translation.findTranslations(false);
        for (Translation translation : partialTranslations) {
            for (int i = 0; i < texts.size(); i++) {
                texts.set(i, texts.get(i).replace(translation.getFrom(), translation.getTo()));
            }
        }
    }
}
