package au.com.ourpillstalkpaid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;

import java.lang.reflect.Field;

/**
 * Created by luongvo on 5/31/18.
 * <p>
 * https://github.com/pardom-zz/ActiveAndroid/issues/279#issuecomment-201948099
 */
public abstract class ParcelableModel extends Model implements Parcelable {
    /**
     * Ensure the ID is set when creating from a parcel
     * Derived classes *must* call through super when overriding this constructor
     *
     * @param in Input parcel
     */
    public ParcelableModel(Parcel in) {
        setId(in);
    }

    /**
     * Empty constructor required by ActiveAndroid (when selecting data from SQLite)
     */
    public ParcelableModel() {
    }

    /**
     * Uses reflection to set the ID field from the parcel
     *
     * @param in Input parcel
     */
    private void setId(Parcel in) {
        try {
            Field idField = Model.class.getDeclaredField("mId");
            idField.setAccessible(true);
            idField.set(this, in.readLong());
        } catch (Exception e) {
            throw new RuntimeException("Reflection failed to get the Active Android ID", e);
        }
    }

    /**
     * Derived classes *must* call through super when overriding this method
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
    }
}
