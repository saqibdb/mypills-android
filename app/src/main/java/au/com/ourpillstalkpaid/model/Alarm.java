package au.com.ourpillstalkpaid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by luongvo on 5/29/18.
 */
@Table(name = "Alarms")
public class Alarm extends ParcelableModel implements Parcelable {

    public static final int REFILL_PRESCRIPTION = 0;
    public static final int TAKE_MEDICINE = 1;

    @Column(name = "mode")
    private int mode;
    @Column(name = "dateTime")
    private Date dateTime;
    @Column(name = "scannedPillId")
    private long scannedPillId;
    @Column(name = "drugName")
    private String drugName;
    @Column(name = "description")
    private String description;

    /**
     * Empty constructor required by ActiveAndroid (when selecting data from SQLite)
     */
    public Alarm() {
    }

    public Alarm(int mode, Date dateTime, long scannedPillId, String drugName, String description) {
        this.mode = mode;
        this.dateTime = dateTime;
        this.scannedPillId = scannedPillId;
        this.drugName = drugName;
        this.description = description;
    }

    protected Alarm(Parcel in) {
        super(in);
        mode = in.readInt();
        dateTime = new Date(in.readLong());
        scannedPillId = in.readLong();
        drugName = in.readString();
        description = in.readString();
    }

    public static final Creator<Alarm> CREATOR = new Creator<Alarm>() {
        @Override
        public Alarm createFromParcel(Parcel in) {
            return new Alarm(in);
        }

        @Override
        public Alarm[] newArray(int size) {
            return new Alarm[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(mode);
        dest.writeLong(dateTime.getTime());
        dest.writeLong(scannedPillId);
        dest.writeString(drugName);
        dest.writeString(description);
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public long getScannedPillId() {
        return scannedPillId;
    }

    public void setScannedPillId(long scannedPillId) {
        this.scannedPillId = scannedPillId;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String formatDateTime() {
        return mode == REFILL_PRESCRIPTION ?
                ScannedPill.formatDateWithTranslation(dateTime) :
                new SimpleDateFormat("hh:mm a").format(dateTime);
    }
}
