package au.com.ourpillstalkpaid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.MyApplication;

@Table(name = "Scanned_Pills", id = "S_No")
public class ScannedPill extends ParcelableModel implements Parcelable {

    private static final String UPDATED_DATE_FORMAT = "hh:mm aa, dd MMMM yyyy";
    public static final String DISPENSED_DATE_FORMAT = "d/M/yyyy";

    @Column(name = "XML")
    private String xml;
    @Column(name = "Name")
    private String name;
    @Column(name = "Date_and_Time")
    private String updatedDate;
    @Column(name = "PharmacyName")
    private String pharmacyName;
    @Column(name = "DispensedDate")
    private String dispensedDate;

    /**
     * Empty constructor required by ActiveAndroid (when selecting data from SQLite)
     */
    public ScannedPill() {
    }

    public ScannedPill(String xml, String name, String pharmacyName, String dispensedDate) {
        this.xml = xml;
        this.name = name;
        setUpdatedDate();
        this.pharmacyName = pharmacyName;
        this.dispensedDate = dispensedDate;
    }

    protected ScannedPill(Parcel in) {
        super(in);
        xml = in.readString();
        name = in.readString();
        updatedDate = in.readString();
        pharmacyName = in.readString();
        dispensedDate = in.readString();
    }

    public static final Creator<ScannedPill> CREATOR = new Creator<ScannedPill>() {
        @Override
        public ScannedPill createFromParcel(Parcel in) {
            return new ScannedPill(in);
        }

        @Override
        public ScannedPill[] newArray(int size) {
            return new ScannedPill[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(xml);
        dest.writeString(name);
        dest.writeString(updatedDate);
        dest.writeString(pharmacyName);
        dest.writeString(dispensedDate);
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getName() {
        return name;
    }

    public String getNameSub() {
        return name.substring(0, name.indexOf(" "));
    }

    public Date getUpdatedDate() {
        SimpleDateFormat format = new SimpleDateFormat(UPDATED_DATE_FORMAT);
        try {
            return format.parse(updatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public void setUpdatedDate() {
        SimpleDateFormat format = new SimpleDateFormat(UPDATED_DATE_FORMAT);
        this.updatedDate = format.format(new Date());
    }

    public String getDispensedDate() {
        return dispensedDate;
    }

    public String formatDispensedDate() {
        SimpleDateFormat format = new SimpleDateFormat(DISPENSED_DATE_FORMAT);
        try {
            Date date = format.parse(dispensedDate);
            return formatDateWithTranslation(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dispensedDate;
    }

    public static String formatDateWithTranslation(Date date) {
        SimpleDateFormat format2 = new SimpleDateFormat("dd yyyy");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String str = format2.format(date);

        int month = calendar.get(Calendar.MONTH);
        String monthName = Constants.MONTH_NAMES.get(month);
/*
        if (!MyApplication.lang.equalsIgnoreCase(Constants.EN)) {
            List<MonthName> monthNames = new Select().from(MonthName.class).execute();
            if (month < monthNames.size() && monthNames.size() == 12) {
                monthName = monthNames.get(month).getName();
            }
            else if (monthNames.size()==1){
                String [] array = monthNames.get(0).getName().split(" ");
                if (month < array.length && array.length == 12){
                    monthName = array[month];
                }
            }
        }
*/
/*
        ArrayList<String> monthList = new ArrayList<>();
        monthList.add(monthName);
        Translation.partialTranslation(monthList);
*/
/*
        if (month == 4) {
            if (MyApplication.lang.equalsIgnoreCase("de")) {
                monthName = "Mai";
            } else if (MyApplication.lang.equalsIgnoreCase("el")) {
                monthName = "Μαΐοc";
            }
        }
*/

        return str.replace(" ", " " + monthName + " ");
    }

    public void setDispensedDate(String dispensedDate) {
        this.dispensedDate = dispensedDate;
    }

    public String getInstructions() {
        String[] items = xml.replace("\n\n", "\n").split("\n");
        return items.length < 2 ?
                "" :
                items[2].trim().replace("/", " per ");
    }
}
