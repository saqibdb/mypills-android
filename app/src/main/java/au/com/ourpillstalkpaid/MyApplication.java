package au.com.ourpillstalkpaid;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import au.com.ourpillstalkpaid.dialogs.NotificationDialog;
import au.com.ourpillstalkpaid.model.Alarm;

import static au.com.ourpillstalkpaid.model.Alarm.REFILL_PRESCRIPTION;
import static au.com.ourpillstalkpaid.model.Alarm.TAKE_MEDICINE;
import static au.com.ourpillstalkpaid.util.PreferencesUtils.getPreference;

public class MyApplication extends Application {

    private static final String TAG = MyApplication.class.getSimpleName();

    public static String firstName = "", lastName = "", gender = "Male", emailAddress = "",
            yearOfBirth = "", postCode = "", countryOfOrigin = "", preferedLang = "",
            allergies = "", emergencyContactNumber = "", lang = Constants.EN;
    public static int spnrIndex = 21;
    public int countrySpnrIndex = 8;

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        ActiveAndroid.setLoggingEnabled(BuildConfig.DEBUG);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Context ctx = getApplicationContext();
        boolean saveInfo = sp.getBoolean("SaveInfo", false);
        boolean saveSpinnerIndex = sp.getBoolean("saveSpinnerIndex", false);
        boolean saveCountrySpnrIndex = sp.getBoolean("saveCountrySpinnerIndex", false);

        if (saveInfo) {
            firstName = getPreference("firstName", ctx);
            lastName = getPreference("lastName", ctx);
            gender = getPreference("gender", ctx);
            emailAddress = getPreference("email", ctx);
            yearOfBirth = getPreference("yob", ctx);
            postCode = getPreference("postcode", ctx);
            countryOfOrigin = getPreference("country", ctx);
            preferedLang = getPreference("lang", ctx);
            allergies = getPreference("allergies", ctx);
            emergencyContactNumber = getPreference("number", ctx);
        }

        if (saveSpinnerIndex) {
            spnrIndex = Integer.parseInt(getPreference("Index", ctx));
            lang = getPreference("myLang", ctx);
        }

        if (saveCountrySpnrIndex) {
            countrySpnrIndex = Integer.parseInt(getPreference("CountrySpnrIndex", ctx));
        }
        loadRefillAlarms();
        loadTakeAlarms();
        checkForNotification();
        new Handler().postDelayed(() -> {
            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);

            for (int i = 0; i < refilAlarm.size(); i++) {
                Alarm alarm = refilAlarm.get(i);
                Date alarmDate = alarm.getDateTime();
                if (alarmDate.getDate() == day && alarmDate.getMonth() == month) {
                    if (activity != null) {
                        new Handler().postDelayed(() -> {
                            new NotificationDialog(activity, alarm.getDrugName() + " " + alarmDate.getDate() + "/" + (alarmDate.getMonth() + 1), R.string.notification_alarm_refill);
                        }, i * 1400);
                    }
                }
            }
        }, 3000);
    }

    private void checkForNotification() {
        Calendar calendar = Calendar.getInstance();
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int mins = calendar.get(Calendar.MINUTE);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;

        for (int i = 0; i < medicineAlarm.size(); i++) {
            Alarm alarm = medicineAlarm.get(i);
            Date alarmDate = alarm.getDateTime();
            if (alarmDate.getMinutes() == mins && alarmDate.getHours() == hours) {
                if (activity != null)
                    new NotificationDialog(activity, alarm.getDrugName() + " " + alarmDate.getHours() + ":" + alarmDate.getMinutes(), R.string.notification_alarm_take);
            }
        }
/*
        for (int i = 0; i < refilAlarm.size(); i++) {
            Alarm alarm = refilAlarm.get(i);
            Date alarmDate = alarm.getDateTime();
            if (alarmDate.getDay() == day && alarmDate.getMonth() == month) {
                if (activity != null)
                    new NotificationDialog(activity, alarm.getDrugName() + " " + alarmDate.getDay() + "/" + alarmDate.getMonth(), R.string.notification_alarm_refill);
            }
        }
*/

        new Handler().postDelayed(() -> {
            checkForNotification();
        }, 1000 * 60);
        loadTakeAlarms();
    }

    public static Activity activity;

    @Override
    public void onTerminate() {
        super.onTerminate();
        ActiveAndroid.dispose();
    }

    private static ArrayList<Alarm> refilAlarm = new ArrayList<>();
    private static ArrayList<Alarm> medicineAlarm = new ArrayList<>();

    private void loadRefillAlarms() {
        From from = new Select().from(Alarm.class).where("mode = ?", REFILL_PRESCRIPTION);
        refilAlarm.addAll(from.orderBy("dateTime asc").execute());
    }

    private void loadTakeAlarms() {
        medicineAlarm.clear();
        From from = new Select().from(Alarm.class).where("mode = ?", TAKE_MEDICINE);
        medicineAlarm.addAll(from.orderBy("dateTime asc").execute());
    }


    public static String abbreviations(String scanText) {
        String splitter[] = scanText.split("</in|in>");
        scanText = " " + scanText + " ";
        Log.e("Abbrevation text", scanText);
        scanText = " " + splitter[1] + " ";
        scanText = scanText.replaceAll("pv", "p v");
        scanText = scanText.replaceAll(" % ", " percent ");
        scanText = scanText.replaceAll(" (?i)1acn ", " one hour before meals and at bedtime ");
        scanText = scanText.replaceAll(" (?i)aa ", " to the affected area ");
        scanText = scanText.replaceAll("ac ", "before meals");
        scanText = scanText.replaceAll("a\\.c", "before meals");
        scanText = scanText.replaceAll(" (?i)a\\.c", "before meals");
        scanText = scanText.replaceAll("auto-inh", "auto inhaler ");

        if (scanText.toLowerCase().contains(" alt d ")) {
            scanText = scanText.replaceAll(" (?i)alt d ", " on alternate days ");
        } else {
            scanText = scanText.replaceAll(" (?i)alt ", " on alternate ");
        }
        if (scanText.toLowerCase().contains(" ex aq ")) {
            scanText = scanText.replaceAll(" (?i)ex aq ", " in water ");
            //  return scanText;
        } else {
            scanText = scanText.replaceAll(" (?i)ex ", " external use only ");
        }
        scanText = scanText.replaceAll(" (?i)aq ", " with water ");
        scanText = scanText.replaceAll(" (?i)bd ", " twice daily ");
        scanText = scanText.replaceAll(" (?i)bid ", " twice daily ");
        scanText = scanText.replaceAll(" (?i)cap ", " capsule ");
        scanText = scanText.replaceAll(" (?i)caps ", " capsules ");
        scanText = scanText.replaceAll(" (?i)cc ", " with food ");
        scanText = scanText.replaceAll(" (?i)cr ", " cream ");
        scanText = scanText.replaceAll(" (?i)crem ", " cream ");
        scanText = scanText.replaceAll(" (?i)crm ", " cream ");
        scanText = scanText.replaceAll(" (?i)d ", " daily ");
        scanText = scanText.replaceAll(" (?i)disp ", " dispensed ");
        scanText = scanText.replaceAll(" (?i)fini ", " until finished ");
//        scanText = scanText.replaceAll(" (?i)g " , " gram ");
        scanText = scanText.replaceAll(" (?i)gutt ", " drops ");
        scanText = scanText.replaceAll(" (?i)h ", " hours ");
        scanText = scanText.replaceAll(" (?i)hs ", " at bedtime ");
        scanText = scanText.replaceAll(" (?i)ex ", " external use only ");
        scanText = scanText.replaceAll(" IM ", " intramuscular ");
        scanText = scanText.replaceAll(" IN ", " intra nasal ");
        scanText = scanText.replaceAll(" (?i)inh ", " inhaler ");
        scanText = scanText.replaceAll(" (?i)-inh ", " -inhaler ");
        scanText = scanText.replaceAll(" (?i)inj ", " injection ");
        scanText = scanText.replaceAll(" IU ", " units ");
        scanText = scanText.replaceAll(" (?i)l ", " left ");
        scanText = scanText.replaceAll(" (?i)linc ", " linctus ");
        scanText = scanText.replaceAll(" (?i)linct ", " linctus ");
        scanText = scanText.replaceAll(" (?i)m ", " in the morning ");
        scanText = scanText.replaceAll(" MDI ", " metered dose inhaler ");
        scanText = scanText.replaceAll(" (?i)mdu ", " as directed by your doctor ");
//        scanText = scanText.replaceAll(" (?i)mg "  ,  " milligram " );
//        scanText = scanText.replaceAll(" (?i) mcg", " micrograms");
        scanText = scanText.replaceAll(" (?i)mist ", " mixture ");
//        scanText = scanText.replaceAll(" (?i)mmol "  ,  " millimoles " );
        scanText = scanText.replaceAll(" (?i)n ", " at night ");
        scanText = scanText.replaceAll(" (?i)oc ", " eye ointment ");
        scanText = scanText.replaceAll(" (?i)occ ", " eye ointment ");
        scanText = scanText.replaceAll(" (?i)od ", " once daily ");
        scanText = scanText.replaceAll(" (?i)oint ", " ointment ");
        scanText = scanText.replaceAll(" (?i)ot ", " ear ");
        scanText = scanText.replaceAll(" (?i)pc ", " after meals ");
        scanText = scanText.replaceAll(" (?i)pess ", " pessary ");
        scanText = scanText.replaceAll(" (?i)pn ", " for pain ");
        scanText = scanText.replaceAll(" (?i)po ", " by mouth ");
        scanText = scanText.replaceAll(" (?i)pr ", " into the rectum ");
        scanText = scanText.replaceAll(" (?i)prn ", " when required ");
        scanText = scanText.replaceAll(" (?i)Ptch ", " patch ");
        scanText = scanText.replaceAll(" (?i)pulv ", " powder ");
        scanText = scanText.replaceAll(" (?i)pv ", " into the vagina ");
        scanText = scanText.replaceAll(" (?i)q12h ", " every 12 hours ");
        scanText = scanText.replaceAll(" (?i)q4h ", " every 4 hours ");
        scanText = scanText.replaceAll(" (?i)q6h ", " every 6 hours ");
        scanText = scanText.replaceAll(" (?i)q8h ", " every 8 hours ");
        scanText = scanText.replaceAll(" (?i)qds ", " four times a day ");
        scanText = scanText.replaceAll(" (?i)qh ", " every hour ");
        scanText = scanText.replaceAll(" (?i)qhs ", " nightly at bedtime ");
        scanText = scanText.replaceAll(" (?i)qid ", " four times a day ");
        scanText = scanText.replaceAll(" (?i)qqh ", " every 4 hours ");
        scanText = scanText.replaceAll(" (?i)r ", " right ");
        scanText = scanText.replaceAll(" (?i)rect ", " rectum ");
        scanText = scanText.replaceAll(" (?i)sl ", " under the tongue ");
        scanText = scanText.replaceAll(" (?i)sos ", " if necessary ");
        scanText = scanText.replaceAll(" (?i)stat ", " immediately ");
        scanText = scanText.replaceAll(" (?i)supp ", " suppository ");
        scanText = scanText.replaceAll(" (?i)syr ", " syrup ");
        if (scanText.toLowerCase().contains(" tab-ec ")) {
            scanText = scanText.replaceAll(" (?i)tab-ec ", " enteric coated ");
        } else {
            scanText = scanText.replaceAll(" (?i)tab ", " tablet ");
        }
        scanText = scanText.replaceAll(" (?i)tabs ", " tablets ");
        scanText = scanText.replaceAll(" (?i)tds ", " three times a day ");
        scanText = scanText.replaceAll(" (?i)tid ", " three times a day ");
        scanText = scanText.replaceAll(" (?i)top ", " on the skin ");
        scanText = scanText.replaceAll(" (?i)uat ", " until all taken ");
        scanText = scanText.replaceAll(" (?i)ung ", " ointment ");
        scanText = scanText.replaceAll(" (?i)utd ", " as directed ");
        scanText = scanText.replaceAll(" (?i)w ", " weekly ");
        scanText = scanText.replaceAll(" (?i)xaq ", " with water ");
        scanText = scanText.replaceAll(" (?i)vag ", " vaginal ");

        splitter[1] = scanText;
        String finalz = "";
        finalz += splitter[0];
        finalz += "in>";
        finalz += splitter[1];
        finalz += "</in";
        finalz += splitter[2];

        return finalz;
    }

    public static String abbreviationsTwo(String scanText) {
        scanText = " " + scanText + " ";

        scanText = scanText.replaceAll("<", " <");
        scanText = scanText.replaceAll(">", " >");

        scanText = scanText.replaceAll(" % ", " percent ");
        scanText = scanText.replaceAll(" auto-inh ", " auto inhaler ");
        scanText = scanText.replaceAll(" (?i)1acn ", " one hour before meals and at bedtime ");
        scanText = scanText.replaceAll("aa ", "to the affected area ");
        scanText = scanText.replaceAll("Auto-Inh ", "Auto Inhaler ");
        scanText = scanText.replaceAll("ac ", " before meals");
        scanText = scanText.replaceAll("a\\.c", "before meals");
        scanText = scanText.replaceAll(" (?i)a\\.c", "before meals");
        if (scanText.toLowerCase().contains(" alt d ")) {
            scanText = scanText.replaceAll(" (?i)alt d ", " on alternate days ");
        } else {
            scanText = scanText.replaceAll(" (?i)alt ", " on alternate ");
        }
        if (scanText.toLowerCase().contains(" ex aq ")) {
            scanText = scanText.replaceAll(" (?i)ex aq ", " in water ");
            //  return scanText;
        } else {
            scanText = scanText.replaceAll(" (?i)ex ", " external use only ");
        }
        scanText = scanText.replaceAll(" (?i)aq ", " with water ");
        scanText = scanText.replaceAll(" (?i)bd ", " twice daily ");
        scanText = scanText.replaceAll(" (?i)bid ", " twice daily ");
        scanText = scanText.replaceAll(" (?i)cap ", " capsule ");
        scanText = scanText.replaceAll(" (?i)caps ", " capsules ");
        scanText = scanText.replaceAll(" (?i)cc ", " with food ");
        scanText = scanText.replaceAll(" (?i)cr ", " cream ");
        scanText = scanText.replaceAll(" (?i)crem ", " cream ");
        scanText = scanText.replaceAll(" (?i)crm ", " cream ");
        scanText = scanText.replaceAll(" (?i)d ", " daily ");
        scanText = scanText.replaceAll(" (?i)disp ", " dispensed ");
        scanText = scanText.replaceAll(" eye-drp ", " eye drops ");
        scanText = scanText.replaceAll(" (?i)fini ", " until finished ");
        scanText = scanText.replaceAll("(?i)amp\\;", "");
//        scanText = scanText.replaceAll(" (?i)g " , " gram ");
        scanText = scanText.replaceAll(" (?i)gutt ", " drops ");
        scanText = scanText.replaceAll(" (?i)h ", " hours ");
        scanText = scanText.replaceAll(" (?i)hr ", " hours ");
        scanText = scanText.replaceAll("(?i)hr ", " hours ");
        scanText = scanText.replaceAll(" (?i)hs ", " at bedtime ");
        scanText = scanText.replaceAll(" (?i)ex ", " external use only ");
        scanText = scanText.replaceAll(" IM ", " intramuscular ");
        scanText = scanText.replaceAll(" IN ", " intra nasal ");
        scanText = scanText.replaceAll(" (?i)inh ", " inhaler ");
        scanText = scanText.replaceAll(" (?i)inj ", " injection ");
        scanText = scanText.replaceAll(" IU ", " units ");
        scanText = scanText.replaceAll(" (?i)l ", " left ");
        scanText = scanText.replaceAll(" (?i)linc ", " linctus ");
        scanText = scanText.replaceAll(" (?i)linct ", " linctus ");
        scanText = scanText.replaceAll(" (?i)m ", " in the morning ");
        scanText = scanText.replaceAll(" met-aero ", " metered aerosol ");
        scanText = scanText.replaceAll(" MDI ", " metered dose inhaler ");
        scanText = scanText.replaceAll(" (?i)mdu ", " as directed by your doctor ");
//        scanText = scanText.replaceAll(" (?i)mg "  ,  " milligram " );
        scanText = scanText.replaceAll(" (?i)mist ", " mixture ");
//        scanText = scanText.replaceAll(" (?i)mmol "  ,  " millimoles " );
        scanText = scanText.replaceAll(" (?i)n ", " at night ");
        scanText = scanText.replaceAll(" (?i)oc ", " eye ointment ");
        scanText = scanText.replaceAll("occ ", "eye ointment ");
        scanText = scanText.replaceAll(" (?i)od ", " once daily ");
        scanText = scanText.replaceAll(" (?i)oint ", " ointment ");
        scanText = scanText.replaceAll(" (?i)ot ", " ear ");
        scanText = scanText.replaceAll(" (?i)pc ", " after meals ");
        scanText = scanText.replaceAll(" (?i)pess ", " pessary ");
        scanText = scanText.replaceAll(" (?i)pn ", " for pain ");
        scanText = scanText.replaceAll(" (?i)po ", " by mouth ");
        scanText = scanText.replaceAll(" (?i)pr ", " into the rectum ");
        scanText = scanText.replaceAll(" (?i)prn ", " when required ");
        scanText = scanText.replaceAll(" (?i)Ptch ", " patch ");
        scanText = scanText.replaceAll(" (?i)pulv ", " powder ");
        scanText = scanText.replaceAll(" (?i)pv ", " into the vagina ");
        scanText = scanText.replaceAll(" (?i)q12h ", " every 12 hours ");
        scanText = scanText.replaceAll(" (?i)q4h ", " every 4 hours ");
        scanText = scanText.replaceAll(" (?i)q6h ", " every 6 hours ");
        scanText = scanText.replaceAll(" (?i)q8h ", " every 8 hours ");
        scanText = scanText.replaceAll(" (?i)qds ", " four times a day ");
        scanText = scanText.replaceAll(" (?i)qh ", " every hour ");
        scanText = scanText.replaceAll(" (?i)qhs ", " nightly at bedtime ");
        scanText = scanText.replaceAll(" (?i)qid ", " four times a day ");
        scanText = scanText.replaceAll(" (?i)qqh ", " every 4 hours ");
        scanText = scanText.replaceAll(" (?i)r ", " right ");
        scanText = scanText.replaceAll(" (?i)rect ", " rectum ");
        scanText = scanText.replaceAll(" (?i)sl ", " under the tongue ");
        scanText = scanText.replaceAll(" (?i)sos ", " if necessary ");
        scanText = scanText.replaceAll(" (?i)stat ", " immediately ");
        scanText = scanText.replaceAll(" (?i)supp ", " suppository ");
        scanText = scanText.replaceAll(" (?i)syr ", " syrup ");
        if (scanText.toLowerCase().contains(" tab-ec ")) {
            scanText = scanText.replaceAll(" (?i)tab-ec ", " enteric coated ");
        } else {
            scanText = scanText.replaceAll(" (?i)tab ", " tablet ");
        }
        scanText = scanText.replaceAll(" (?i)tabs ", " tablets ");
        scanText = scanText.replaceAll(" (?i)tds ", " three times a day ");
        scanText = scanText.replaceAll(" (?i)tid ", " three times a day ");
        scanText = scanText.replaceAll(" (?i)top ", " on the skin ");
        scanText = scanText.replaceAll(" (?i)uat ", " until all taken ");
        scanText = scanText.replaceAll(" (?i)ung ", " ointment ");
        scanText = scanText.replaceAll(" (?i)utd ", " as directed ");
        scanText = scanText.replaceAll(" (?i)w ", " weekly ");
        scanText = scanText.replaceAll(" (?i)xaq ", " with water ");
        scanText = scanText.replaceAll(" (?i)vag ", " vaginal ");
        scanText = scanText.replaceAll("(?i)null", "  ");
        scanText = scanText.replaceAll(" (?i)\\d+u", " $0uunits").replace("uuunits", " units");
        scanText = scanText.replaceAll(" (?i)x1d", " for 1 day");
        scanText = scanText.replaceAll(" (?i)x2d", " for 2 days");
        scanText = scanText.replaceAll(" (?i)x3d", " for 3 days");
        scanText = scanText.replaceAll(" (?i)x4d", " for 4 days");
        scanText = scanText.replaceAll(" (?i)x5d", " for 5 days");
        scanText = scanText.replaceAll(" (?i)x6d", " for 6 days");
        scanText = scanText.replaceAll(" (?i)x7d", " for 7 days");
        scanText = scanText.replaceAll(" (?i)x14d", " for 14 days");
        return scanText;
    }

    public static String abbreviationsUnitsToSpeak(String text) {
        Log.d(TAG, "before abbreviate = " + text);
        // fix tts treats a dash between two numbers as a minus instead of as a dash
        text = text.replaceAll("(\\d+)\\s*-\\s*(\\d+)", "$1" + " to " + "$2");

        text = text.replaceAll("(?i) mcg", " micrograms");
        text = text.replaceAll("(?i)\\d+mcg", " $0mmicrograms").replace("mcgmmicrograms", " micrograms");
        text = text.replaceAll("(?i) mmol", " millimoles");
        text = text.replaceAll("(?i)\\d+mmol", " $0millimoles").replace("mmolmillimoles", " millimoles");
        text = text.replaceAll("(?i) g", " grams");
        text = text.replaceAll("(?i)\\d+g", " $0grams").replace("ggrams", " grams");
        text = text.replaceAll("(?i) mcl", " microlitres");
        text = text.replaceAll("(?i)\\d+mcl", " $0mmicrolitres").replace("mclmmicrolitres", " microlitres");
        text = text.replaceAll("(?i) mg", " milligrams");
        text = text.replaceAll("(?i)\\d+mg", " $0mmilligrams").replace("mgmmilligrams", " milligrams");
        ;
        text = text.replaceAll("(?i) ml", " millilitres");
        text = text.replaceAll("(?i)\\d+ml", " $0mmillilitres").replace("mlmmillilitres", " millilitres");

        Log.d(TAG, "after abbreviate = " + text);
        return text;
    }

    public static int getYearString() {
        Calendar c = Calendar.getInstance(TimeZone.getDefault());

        int yearInt = c.get(Calendar.YEAR);
        return yearInt;
    }
}
