package au.com.ourpillstalkpaid;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static final String EN = "en";
    public static final String DISPENSED = "Dispensed:";
    public static final String ALARM_ID = "ALARM_ID";
    public static final List<String> MONTH_NAMES = new ArrayList<String>() {
        {
            add("January");
            add("February");
            add("March");
            add("April");
            add("May");
            add("June");
            add("July");
            add("August");
            add("September");
            add("October");
            add("November");
            add("December");
        }
    };
}
