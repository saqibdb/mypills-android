package au.com.ourpillstalkpaid;

public class Config {

//    public static final String MICROSOFT_TRANSLATOR_API_ID = "com_steve_OurPillsTalk007";
//    public static final String MICROSOFT_TRANSLATOR_API_SECRET = "QR2+jqRWpURZXpfEOMD7h6+9Nqy83hYBw3UuKcYaQIw=";

    public static final String KEY_TRANSLATE_URL = "https://translation.googleapis.com/language/translate/v2";
    public static final String KEY_GOOGLE_API_KEY = "AIzaSyC5RFYhvGr-UHYM6Rhfgjc3NSgPk-BLaao";

    public static final String MICROSOFT_TTS_API_KEY = "f4331c43fcc1435ca9486573ea1175d6";
    public static final String MICROSOFT_TTS_API_KEY2 = "14f2767618c04e33b4061d10d7c88abd";
    public static final String MICROSOFT_TTS_TOKEN_URL = "https://westus.api.cognitive.microsoft.com/sts/v1.0/issueToken";
    public static final String MICROSOFT_TTS_SERVICE_URL = "https://westus.tts.speech.microsoft.com/cognitiveservices/v1";

    public static final int MAX_SCAN_ITEMS = 20;
    public static final String HEALTH_INFO_URL = "https://www.healthdirect.gov.au/";
    public static final String URL_INSERT_USER = "https://www.ourpillstalk.com.au/submission/insertuser.php";
    public static final String URL_INSERT_SCAN = "https://www.ourpillstalk.com.au/submission/insertscan.php";
    public static final String LANG_TRANS_DATA_URL = "https://www.ourpillstalk.com.au/translations/get.php?l=";
}
