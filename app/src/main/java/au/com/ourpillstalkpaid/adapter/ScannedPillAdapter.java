package au.com.ourpillstalkpaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.model.ScannedPill;

public class ScannedPillAdapter extends BaseAdapter {

    private List<ScannedPill> scannedPills;
    private LayoutInflater inflater;

    private ActionListener actionListener;

    public ScannedPillAdapter(List<ScannedPill> scannedPills, Context context, ActionListener actionListener) {
        this.scannedPills = scannedPills;
        this.actionListener = actionListener;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return scannedPills.isEmpty() ? 1 : scannedPills.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder viewHolder;

        if (view == null) {
            view = inflater.inflate(R.layout.item_scanned_pill, null);
            viewHolder = new ViewHolder();
            viewHolder.text = view.findViewById(R.id.txt_listItem);
            viewHolder.image = view.findViewById(R.id.img_delete);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.image.setTag(position);
        viewHolder.image.setOnClickListener(v -> {
            int position12 = (int) v.getTag();
            if (actionListener != null) {
                actionListener.onDeleteClicked(scannedPills.get(position12));
            }
        });

        viewHolder.text.setTag(position);
        viewHolder.text.setOnClickListener(v -> {
            int position1 = (int) v.getTag();
            if (!"No Scanned Data".equals(((TextView) v).getText().toString()) && actionListener != null) {
                actionListener.onItemClicked(scannedPills.get(position1));
            }
        });

        if (scannedPills.isEmpty()) {
            viewHolder.text.setText("No Scanned Data");
            viewHolder.image.setVisibility(View.GONE);
        } else {
            viewHolder.image.setVisibility(View.VISIBLE);
            ScannedPill scannedPill = scannedPills.get(position);
            viewHolder.text.setText(scannedPill.getName().replace("/", " per ")
                    + "\n" + Constants.DISPENSED + " " + scannedPill.formatDispensedDate());
        }
        return view;
    }

    public interface ActionListener {

        void onItemClicked(ScannedPill scannedPill);

        void onDeleteClicked(ScannedPill scannedPill);
    }

    private static class ViewHolder {
        TextView text;
        ImageView image;
    }
}
