package au.com.ourpillstalkpaid.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.ItemAlarmBinding;
import au.com.ourpillstalkpaid.model.Alarm;
import au.com.ourpillstalkpaid.model.ScannedPill;

/**
 * Created by luongvo on 5/29/18.
 */
public class AllAlarmsAdapter extends RecyclerView.Adapter<AllAlarmsAdapter.ViewHolder> {

    private ScannedPill scannedPill;
    private List<Alarm> alarms;
    private ActionListener actionListener;

    public AllAlarmsAdapter(ScannedPill scannedPill, List<Alarm> alarms, ActionListener actionListener) {
        this.scannedPill = scannedPill;
        this.alarms = alarms;
        this.actionListener = actionListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_alarm, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.view.setTag(holder);
        holder.binding.setAlarm(alarms.get(position));
        holder.binding.setScannedPill(scannedPill);
        holder.binding.setListener(actionListener);
    }

    @Override
    public int getItemCount() {
        return alarms.size();
    }

    public interface ActionListener {

        void onEditClicked(Alarm alarm);

        void onDeleteClicked(Alarm alarm);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private ItemAlarmBinding binding;

        ViewHolder(ItemAlarmBinding binding) {
            super(binding.getRoot());
            this.view = binding.getRoot();
            this.binding = binding;
        }
    }
}
