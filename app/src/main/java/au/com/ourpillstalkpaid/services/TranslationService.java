package au.com.ourpillstalkpaid.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import au.com.ourpillstalkpaid.Config;
import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;

/**
 * Created by kamal on 8/17/2017.
 */

public class TranslationService extends AsyncTask<Void, Void, String[]> {

    private static final String KEY_DATA = "data";
    private static final String KEY_TRANSLATIONS = "translations";
    private static final String KEY_TRANSLATEDTEXT = "translatedText";
    private static final String KEY_ERROR = "error";
    private static final String KEY_CODE = "code";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_Q = "q";
    private static final String KEY_TARGET = "target";
    private static final String KEY_KEY = "key";

    private Context context;
    private ProgressDialog dialog;
    private List<String> texts;
    private List<View> views;
    private int responseCode;

    protected TranslationService(Context context, List<String> texts, boolean showLoading) {
        this(context, texts, null, showLoading);
    }

    protected TranslationService(Context context, List<String> texts, List<View> views, boolean showLoading) {
        this.context = context;
        this.texts = texts;
        this.views = views;
        if (showLoading) {
            dialog = new ProgressDialog(context);
            dialog.setMessage(context.getString(R.string.loading_language));
        }
    }

    @Override
    protected void onPreExecute() {
        if (dialog != null) {
            dialog.show();
        }
    }

    @Override
    protected String[] doInBackground(Void... params) {
        String[] translatedTexts = new String[texts.size()];

        MultipartUtility multipartUtility = null;
        try {
            for (int i = 0; i < texts.size(); i++) {
                translatedTexts[i] = "";
                Log.d("Translating: ", texts.get(i));

                multipartUtility = new MultipartUtility(Config.KEY_TRANSLATE_URL, "utf-8");
                multipartUtility.addFormField(KEY_Q, texts.get(i));
                multipartUtility.addFormField(KEY_TARGET, MyApplication.lang);
                multipartUtility.addFormField(KEY_KEY, Config.KEY_GOOGLE_API_KEY);

                String response = multipartUtility.finish();
                Log.d("response", response.toString());

                try {
                    JSONObject lJsonObject = new JSONObject(response);
                    if (lJsonObject.has(KEY_DATA)) {
                        JSONObject lLocalJsobObject = lJsonObject.getJSONObject(KEY_DATA);
                        JSONArray lJsonArray = lLocalJsobObject.getJSONArray(KEY_TRANSLATIONS);
                        for (int count = 0; count < lJsonArray.length(); count++) {
                            JSONObject lJsonLocalObject = lJsonArray.getJSONObject(count);
                            if (lJsonLocalObject.has(KEY_TRANSLATEDTEXT)) {
                                translatedTexts[i] = lJsonLocalObject.get(KEY_TRANSLATEDTEXT).toString();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

            if (multipartUtility != null) {
                responseCode = multipartUtility.getResponseCode();
            }
        }

        return translatedTexts;
    }

    @Override
    protected void onPostExecute(String[] translatedTexts) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        if (responseCode == HttpURLConnection.HTTP_FORBIDDEN) {
            Toast.makeText(context, R.string.translation_service_is_forbidden, Toast.LENGTH_SHORT).show();
            return;
        }

        if (views != null && !views.isEmpty()) {
            for (int i = 0; i < views.size(); i++) {
                if (views.get(i) instanceof TextView) {
                    ((TextView) views.get(i)).setText(translatedTexts[i]);
                } else if (views.get(i) instanceof Button) {
                    ((Button) views.get(i)).setText(translatedTexts[i]);
                } else if (views.get(i) instanceof Switch) {
                    ((Switch) views.get(i)).setText(translatedTexts[i]);
                } else if (views.get(i) instanceof RadioButton) {
                    ((RadioButton) views.get(i)).setText(translatedTexts[i]);
                } else if (views.get(i) instanceof ImageView) {
                    views.get(i).setTag(translatedTexts[i]);
                }
            }
        }
    }

    public static void translateViews(List<View> views, Context context) {
        List<String> texts = new ArrayList<>();
        for (View view : views) {
            if (view instanceof TextView) {
                texts.add(((TextView) view).getText().toString());
            }
        }
        new TranslationService(context, texts, views, true).execute();
    }
}
