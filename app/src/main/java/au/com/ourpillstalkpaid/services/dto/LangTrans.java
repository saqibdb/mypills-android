package au.com.ourpillstalkpaid.services.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

import au.com.ourpillstalkpaid.model.Translation;

/**
 * Created by luongvo on 7/24/18.r
 */
@Root
public class LangTrans {

    @Element(name = "Lang")
    private String lang;
    @Element(name = "Status")
    private String status;
    @Element(name = "UpdatedDate")
    private String updatedDate;
    @ElementList(name = "FullTranslations")
    private List<Translation> fullTranslations;
    @ElementList(name = "PartialTranslations")
    private List<Translation> partialTranslations;

    public String getLang() {
        return lang;
    }

    public String getStatus() {
        return status;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public List<Translation> getFullTranslations() {
        return fullTranslations;
    }

    public List<Translation> getPartialTranslations() {
        return partialTranslations;
    }
}
