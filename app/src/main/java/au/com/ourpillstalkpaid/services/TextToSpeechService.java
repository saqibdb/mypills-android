package au.com.ourpillstalkpaid.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.microsoft.speech.tts.Synthesizer;
import com.microsoft.speech.tts.Voice;

import java.util.Arrays;
import java.util.List;

import au.com.ourpillstalkpaid.Config;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.util.Utils;

public class TextToSpeechService {

    private static final String TAG = TextToSpeechService.class.getSimpleName();

    private Context context;
    private SharedPreferences sp;

    private TextToSpeech tts;
    private Synthesizer micTTS;
    private boolean micTTSSpeaking;
    private int queueSum = 0;
    private int queueIndex = 0;

    private static TextToSpeechService instance;

    public static TextToSpeechService getInstance(Context context, TextToSpeech.OnInitListener listener) {
        Log.d(TAG, "getInstance");
        instance = new TextToSpeechService(context, listener);
        return instance;
    }

    private TextToSpeechService(Context context, TextToSpeech.OnInitListener listener) {
        this.context = context;
        sp = PreferenceManager.getDefaultSharedPreferences(context);

        tts = new TextToSpeech(context, listener);

        micTTS = new Synthesizer(Config.MICROSOFT_TTS_API_KEY, Config.MICROSOFT_TTS_TOKEN_URL, Config.MICROSOFT_TTS_SERVICE_URL);
        micTTS.SetServiceStrategy(Synthesizer.ServiceStrategy.AlwaysService);
    }

    public void resetQueue() {
        queueSum = 0;
        queueIndex = 0;
    }

    /**
     * @return available
     */
    public boolean speak(@Nullable String text, String languageCode) {
        if (instance == null || tts == null || micTTS == null) return true;
        if (text == null) text = "null";

        int state = checkSpeechProviderAvailable(languageCode);
        switch (state) {
            case 1:
                boolean useAlternative = sp.getBoolean("speech_alternative_" + languageCode, false);
                if (useAlternative) {
                    speakAzure(text, languageCode);
                } else {
                    speakAndroid(text, languageCode);
                }
                break;
            case 2:
                speakAndroid(text, languageCode);
                break;
            case 3:
                speakAzure(text, languageCode);
                break;
        }
        return state != 0;
    }

    private void speakAndroid(@NonNull String text, String languageCode) {
        if (tts == null) return;
        Log.d(TAG, "default speak: " + text);

        tts.setLanguage(Utils.toLocale(languageCode));
        tts.setSpeechRate(0.8f);
        if (tts.speak(text, TextToSpeech.QUEUE_ADD, null) != TextToSpeech.SUCCESS) {
            Toast.makeText(context, R.string.speech_error, Toast.LENGTH_LONG).show();
        }
    }

    private void speakAzure(@NonNull String text, String languageCode) {
        if (micTTS == null) return;
        Log.d(TAG, "azure speak: " + text);

        Voice voice = Voice.VOICES.get(languageCode);
        if (voice != null) {
            micTTS.SetVoice(voice, null);

            queueSum++;
            CustomHandler customHandler = new CustomHandler();
            customHandler.index = queueSum;
            customHandler.postDelayed(() -> {
                // asynchronous, download speech audio
                byte[] data = micTTS.Speak(text);

                // waiting for tts speak done
                Handler h = new Handler();
                Runnable r = new Runnable() {
                    public void run() {
                        if (!isSpeaking() && queueIndex >= customHandler.index - 1) {
                            if (micTTS != null) {
                                if (!micTTS.isSoundDataAvailable(data)) {
                                    Toast.makeText(context, R.string.speech_error, Toast.LENGTH_LONG).show();
                                } else {
                                    micTTSSpeaking = true;
                                    micTTS.playSound(data, () -> {
                                        Log.d(TAG, "Azure TTS speech done!!!");
                                        micTTSSpeaking = false;
                                    });
                                    queueIndex++;
                                }
                            }
                        } else {
                            Log.d(TAG, "Azure tts waiting for previous TTS done");
                            h.postDelayed(this, 200);
                        }
                    }
                };
                h.postDelayed(r, 200);
            }, 0);
        }
    }

    public void stopSpeak() {
        Log.d(TAG, "stopSpeak");
        if (tts != null) tts.stop();
        if (micTTS != null) micTTS.stopSound();
    }

    public void shutdown() {
        stopSpeak();

        Log.d(TAG, "shutdown");
        if (tts != null) tts.shutdown();
        tts = null;
        micTTS = null;
        instance = null;
    }

    /**
     * @return 0 is none, 1 is both, 2 is android, 3 is azure
     */
    public int checkSpeechProviderAvailable(String languageCode) {
        List<String> exceptions = Arrays.asList(context.getResources().getStringArray(R.array.language_code_array_excepts));
        List<String> androidExceptions = Arrays.asList(context.getResources().getStringArray(R.array.android_tts_language_code_array_excepts));
        List<String> azureExceptions = Arrays.asList(context.getResources().getStringArray(R.array.azure_tts_language_code_array_excepts));

        int result = tts.setLanguage(Utils.toLocale(languageCode));
        boolean hasAndroid = result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED
                && !exceptions.contains(languageCode)
                && !androidExceptions.contains(languageCode);

        boolean hasAzure = Voice.VOICES.get(languageCode) != null
                && !exceptions.contains(languageCode)
                && !azureExceptions.contains(languageCode);

        if (!hasAndroid && !hasAzure) return 0;
        if (hasAndroid && hasAzure) return 1;
        if (hasAndroid) return 2;
        return 3;
    }

    private boolean isSpeaking() {
        return (tts != null && tts.isSpeaking()) || micTTSSpeaking;
    }

    static class CustomHandler extends Handler {

        int index;
    }
}
