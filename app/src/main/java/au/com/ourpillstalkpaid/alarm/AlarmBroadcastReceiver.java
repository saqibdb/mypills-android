package au.com.ourpillstalkpaid.alarm;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import au.com.ourpillstalkpaid.util.AlarmUtils;

/**
 * @author luongvo
 */

public class AlarmBroadcastReceiver extends android.content.BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, AlarmService.class));

        // start alarms again
        AlarmUtils.setAlarms(context);
        Log.d(AlarmBroadcastReceiver.class.getSimpleName(), "start alarms again");
    }
}
