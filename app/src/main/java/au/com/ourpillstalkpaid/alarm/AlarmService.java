package au.com.ourpillstalkpaid.alarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.activeandroid.query.Select;

import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.activities.MainActivity;
import au.com.ourpillstalkpaid.model.Alarm;

/**
 * @author luongvo
 */

public class AlarmService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            long alarmId = intent.getLongExtra(Constants.ALARM_ID, -1);
            Alarm alarm = new Select().from(Alarm.class).where("id = ?", alarmId).executeSingle();
            if (alarm != null) {
                createNotification(alarm);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void createNotification(Alarm alarm) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String contentTitle = alarm.getMode() == Alarm.REFILL_PRESCRIPTION ?
                sp.getString("notification_alarm_refill", getString(R.string.notification_alarm_refill)) :
                sp.getString("notification_alarm_take", getString(R.string.notification_alarm_take));

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT);
        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.bellring);  //Here is FILE_NAME is the name of file that you want to play

        Uri uri = Uri.parse("android.resource://au.com.ourpillstalkpaid/" + R.raw.bellring);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setAutoCancel(true)
                .setSound(sound, Notification.VISIBILITY_PUBLIC)
                .setContentTitle(contentTitle)
                .setContentText(alarm.getMode() == Alarm.REFILL_PRESCRIPTION ?
                        alarm.getDrugName() + " - " + alarm.formatDateTime() :
                        alarm.formatDateTime() + " - " + alarm.getDrugName())
                .setContentIntent(pendingIntent);

        notificationBuilder.setVibrate(new long[]{1000});

        // send
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(alarm.getId().intValue(), notificationBuilder.build());
        MediaPlayer.create(this, R.raw.bellring).start();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

}
