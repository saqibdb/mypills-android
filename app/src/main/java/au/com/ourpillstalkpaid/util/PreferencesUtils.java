package au.com.ourpillstalkpaid.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesUtils {

    public static final String USER_ID = "userID";

    public static void setPreference(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getPreference(String key, Context context, String defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, defaultValue);
    }

    public static String getPreference(String key, Context context) {
        return getPreference(key, context, null);
    }

    public static String getPreferenceUserId(Context context) {
        return getPreference(USER_ID, context, "0");
    }
}
