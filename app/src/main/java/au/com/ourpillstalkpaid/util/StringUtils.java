package au.com.ourpillstalkpaid.util;

public class StringUtils {

    public static String trim(String s) {
        return s == null ? "" : s.trim();
    }

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }
}
