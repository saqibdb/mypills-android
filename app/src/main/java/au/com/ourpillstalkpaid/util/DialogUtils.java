package au.com.ourpillstalkpaid.util;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.Calendar;

/**
 * Created by luongvo on 5/31/18.
 */
public class DialogUtils {

    public static void showDatePickerDialog(@NonNull Context context, @NonNull Calendar calendar,
                                            DatePickerDialog.OnDateSetListener listener) {
        new DatePickerDialog(context, listener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        ).show();
    }

    public static void showTimePickerDialog(@NonNull Context context, @NonNull Calendar calendar,
                                            TimePickerDialog.OnTimeSetListener listener) {
        new TimePickerDialog(context, listener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), false)
                .show();
    }
}
