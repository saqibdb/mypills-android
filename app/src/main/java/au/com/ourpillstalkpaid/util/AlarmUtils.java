package au.com.ourpillstalkpaid.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.activeandroid.query.Select;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.alarm.AlarmService;
import au.com.ourpillstalkpaid.model.Alarm;

/**
 * @author luongvo
 */

public class AlarmUtils {

    private static final long DAY_MS = 24 * 60 * 60 * 1000;

    private AlarmUtils() {
        throw new AssertionError();
    }

    public static void setAlarms(Context context) {
        List<Alarm> alarms = new Select().from(Alarm.class).execute();
        for (Alarm alarm : alarms) {
            setAlarm(context, alarm);
        }
    }

    public static void setAlarm(Context context, Alarm alarm) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
        if (alarm.getMode() == Alarm.REFILL_PRESCRIPTION) {
            if (alarm.getDateTime().getTime() >= removeTime(new Date()).getTime()) {
                alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getDateTime().getTime(),
                        getPendingIntent(context, alarm.getId()));
            }
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(alarm.getDateTime());
            calendar.set(Calendar.SECOND, 00);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), DAY_MS,
                    getPendingIntent(context, alarm.getId()));
        }

        Log.i("setAlarm", "alarmId = " + alarm.getId());
    }

    public static void cancelAlarm(Context context, long alarmId) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(getPendingIntent(context, alarmId));

        Log.i("cancelAlarm", "alarmId = " + alarmId);
    }

    private static PendingIntent getPendingIntent(Context context, long alarmId) {
        Intent intent = new Intent(context, AlarmService.class);
        intent.putExtra(Constants.ALARM_ID, alarmId);
        return PendingIntent.getService(context, (int) alarmId, intent, 0);
    }

    private static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
