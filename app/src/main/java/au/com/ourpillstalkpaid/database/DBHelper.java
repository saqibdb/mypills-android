package au.com.ourpillstalkpaid.database;

import android.content.Context;

import com.activeandroid.query.Select;

import java.util.Collections;
import java.util.List;

import au.com.ourpillstalkpaid.model.Alarm;
import au.com.ourpillstalkpaid.model.ScannedPill;
import au.com.ourpillstalkpaid.util.AlarmUtils;

public class DBHelper {

    public static ScannedPill findOne(String name, String instruction) {
        List<ScannedPill> scannedPills = findAll();
        for (ScannedPill scannedPill : scannedPills) {
            String xml = scannedPill.getXml().replace("\n\n", "\n");
            String[] items = xml.split("\n");
            if (name.equals(scannedPill.getName()) && instruction.equals(items[2].trim())) {
                return scannedPill;
            }
        }
        return null;
    }

    public static List<ScannedPill> findAll() {
        List<ScannedPill> scannedPills = new Select().from(ScannedPill.class).execute();

        // sort
        Collections.sort(scannedPills, (o1, o2) -> o2.getUpdatedDate().compareTo(o1.getUpdatedDate()));

        return scannedPills;
    }

    public static void deleteOldScans(Context context, int max) {
        List<ScannedPill> scannedPills = findAll();
        for (int i = max; i < scannedPills.size(); i++) {
            deleteAlarmsOfPill(context, scannedPills.get(i)).delete();
        }
    }

    public static void deleteAlarm(Context context, Alarm alarm) {
        AlarmUtils.cancelAlarm(context, alarm.getId());
        alarm.delete();
    }

    public static ScannedPill deleteAlarmsOfPill(Context context, ScannedPill scannedPill) {
        List<Alarm> alarms = new Select().from(Alarm.class).where("scannedPillId = ?", scannedPill.getId()).execute();
        for (Alarm alarm : alarms) {
            deleteAlarm(context, alarm);
        }
        return scannedPill;
    }

    public static ScannedPill deleteAlarmsOfPill(Context context, ScannedPill scannedPill, int mode) {
        List<Alarm> alarms = new Select().from(Alarm.class).where("scannedPillId = ?", scannedPill.getId())
                .and("mode = ?", mode).execute();
        for (Alarm alarm : alarms) {
            deleteAlarm(context, alarm);
        }
        return scannedPill;
    }
}
