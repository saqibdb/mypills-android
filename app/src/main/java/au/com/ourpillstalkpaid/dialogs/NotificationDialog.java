package au.com.ourpillstalkpaid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.TextView;

import au.com.ourpillstalkpaid.R;

public class NotificationDialog extends Dialog {

    public NotificationDialog(@NonNull Context context, String s, int string) {
        super(context);
        setContentView(R.layout.dialog);
        setCancelable(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ((TextView) findViewById(R.id.titleTv)).setText(context.getString(string));
        ((TextView) findViewById(R.id.textTv)).setText(s);
        ((TextView) findViewById(R.id.okTv)).setOnClickListener(view -> {
            dismiss();
        });
        MediaPlayer.create(context, R.raw.bellring).start();
        show();
    }
}
