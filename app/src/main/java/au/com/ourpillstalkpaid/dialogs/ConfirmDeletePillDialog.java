package au.com.ourpillstalkpaid.dialogs;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import au.com.ourpillstalkpaid.BR;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.DialogConfirmDeletePillBinding;
import au.com.ourpillstalkpaid.model.Alarm;
import au.com.ourpillstalkpaid.model.ScannedPill;

public class ConfirmDeletePillDialog extends DialogFragment {

    private View view;
    private DialogConfirmDeletePillBinding binding;
    private Callback callback;

    @Nullable
    private ScannedPill scannedPill;
    @Nullable
    private Alarm alarm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dialog_confirm_delete_pill, container, false);
            view = binding.getRoot();
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        binding.setVariable(BR.scannedPill, scannedPill);
        binding.setVariable(BR.alarm, alarm);

        binding.btDelete.setOnClickListener(view1 -> {
            if (callback != null) {
                callback.onDelete();
            }
            dismiss();
        });

        binding.btCancel.setOnClickListener(view12 -> dismiss());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        view = null;
        binding.unbind();
        binding = null;
    }

    public ConfirmDeletePillDialog setScannedPill(ScannedPill scannedPill) {
        this.scannedPill = scannedPill;
        return this;
    }

    public ConfirmDeletePillDialog setAlarm(Alarm alarm) {
        this.alarm = alarm;
        return this;
    }

    public ConfirmDeletePillDialog setCallback(Callback callback) {
        this.callback = callback;
        return this;
    }

    public interface Callback {

        void onDelete();
    }
}
