package au.com.ourpillstalkpaid.activities;

import android.os.Bundle;

import com.journeyapps.barcodescanner.CaptureActivity;

import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;

/**
 * Created by luongvo on 3/25/18.
 */

public class CustomCaptureActivity extends CaptureActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        findViewById(R.id.bt_cancel).setOnClickListener(view -> finish());
    }
    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activity=null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity=this;
    }
}
