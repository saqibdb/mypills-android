package au.com.ourpillstalkpaid.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import au.com.ourpillstalkpaid.BR;
import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.adapter.AllAlarmsAdapter;
import au.com.ourpillstalkpaid.database.DBHelper;
import au.com.ourpillstalkpaid.databinding.ActivityAlarmsBinding;
import au.com.ourpillstalkpaid.dialogs.ConfirmDeletePillDialog;
import au.com.ourpillstalkpaid.model.Alarm;
import au.com.ourpillstalkpaid.model.ScannedPill;
import au.com.ourpillstalkpaid.services.TranslationService;
import au.com.ourpillstalkpaid.util.AlarmUtils;
import au.com.ourpillstalkpaid.util.DialogUtils;
import au.com.ourpillstalkpaid.util.Utils;

import static au.com.ourpillstalkpaid.model.Alarm.REFILL_PRESCRIPTION;
import static au.com.ourpillstalkpaid.model.Alarm.TAKE_MEDICINE;

public class AlarmsActivity extends AppCompatActivity implements AllAlarmsAdapter.ActionListener {

    private static final int REQUEST_EDIT = 1;

    private ActivityAlarmsBinding binding;
    private ScannedPill scannedPill;

    private List<Alarm> refillAlarms = new ArrayList<>();
    private List<Alarm> takeAlarms = new ArrayList<>();
    private AllAlarmsAdapter refillAdapter;
    private AllAlarmsAdapter takeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_alarms);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        scannedPill = getIntent().getParcelableExtra("ScannedPill");

        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activity = null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
        if (Utils.hasConnection(this)) {
            List<View> views = new ArrayList<>();
            views.add(binding.tvAlarmsForMedicine);
            views.add(binding.tvRefillAlarms);
            views.add(binding.tvTakeAlarms);
            views.add(binding.tvEmptyRefill);
            views.add(binding.tvEmptyTake);
            TranslationService.translateViews(views, this);
        }
    }

    private void initUI() {
        binding.setVariable(BR.scannedPill, scannedPill);

        refillAdapter = new AllAlarmsAdapter(scannedPill, refillAlarms, this);
        binding.lvRefill.setAdapter(refillAdapter);
        takeAdapter = new AllAlarmsAdapter(scannedPill, takeAlarms, this);
        binding.lvTake.setAdapter(takeAdapter);

        loadRefillAlarms();
        loadTakeAlarms();

        binding.ivAddRefill.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            DialogUtils.showDatePickerDialog(this, calendar, (view, year, month, dayOfMonth) -> {
                Alarm alarm = buildAlarm(REFILL_PRESCRIPTION, new GregorianCalendar(year, month, dayOfMonth).getTime());
                alarm.save();
                loadRefillAlarms();
                AlarmUtils.setAlarm(this, alarm);
            });
        });

        binding.ivAddTake.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            DialogUtils.showTimePickerDialog(this, calendar, (view, hourOfDay, minute) -> {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                Date time = calendar.getTime();

                Alarm alarm = buildAlarm(TAKE_MEDICINE, time);
                // check duplicated
                for (Alarm al : takeAlarms) {
                    if (al.formatDateTime().equals(alarm.formatDateTime())) {
                        Toast.makeText(this, R.string.duplicated_alarm, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                alarm.save();
                loadTakeAlarms();

                AlarmUtils.setAlarm(this, alarm);
            });
        });
    }

    @Override
    public void onEditClicked(Alarm alarm) {
        Intent intent = new Intent(this, EditAlarmActivity.class);
        intent.putExtra("Alarm", alarm);
        startActivityForResult(intent, REQUEST_EDIT);
    }

    @Override
    public void onDeleteClicked(Alarm alarm) {
        new ConfirmDeletePillDialog()
                .setAlarm(alarm)
                .setCallback(() -> {
                    int mode = alarm.getMode();
                    DBHelper.deleteAlarm(this, alarm);

                    if (mode == REFILL_PRESCRIPTION) loadRefillAlarms();
                    else loadTakeAlarms();
                }).show(getSupportFragmentManager(), null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_EDIT && resultCode == RESULT_OK) {
            loadRefillAlarms();
            loadTakeAlarms();
        }
    }

    private void loadRefillAlarms() {
        From from = new Select().from(Alarm.class).where("mode = ?", REFILL_PRESCRIPTION);
        if (scannedPill != null) {
            from.and("scannedPillId = ?", scannedPill.getId());
        }
        refillAlarms.clear();
        refillAlarms.addAll(from.orderBy("dateTime asc").execute());

        refillAdapter.notifyDataSetChanged();
        binding.tvEmptyRefill.setVisibility(refillAlarms.isEmpty() ? View.VISIBLE : View.GONE);
        binding.ivAddRefill.setVisibility(scannedPill != null && refillAlarms.isEmpty() ? View.VISIBLE : View.GONE);
    }

    private void loadTakeAlarms() {
        From from = new Select().from(Alarm.class).where("mode = ?", TAKE_MEDICINE);
        if (scannedPill != null) {
            from.and("scannedPillId = ?", scannedPill.getId());
        }
        takeAlarms.clear();
        takeAlarms.addAll(from.orderBy("dateTime asc").execute());

        takeAdapter.notifyDataSetChanged();
        binding.tvEmptyTake.setVisibility(takeAlarms.isEmpty() ? View.VISIBLE : View.GONE);
    }

    private Alarm buildAlarm(int mode, Date dateTime) {
        return new Alarm(mode,
                dateTime,
                scannedPill.getId(),
                scannedPill.getName(),
                scannedPill.getInstructions());
    }
}
