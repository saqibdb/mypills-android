package au.com.ourpillstalkpaid.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.com.ourpillstalkpaid.Config;
import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.ActivityUserInfoBinding;
import au.com.ourpillstalkpaid.services.CustomRequest;
import au.com.ourpillstalkpaid.util.Utils;

import static au.com.ourpillstalkpaid.util.PreferencesUtils.USER_ID;
import static au.com.ourpillstalkpaid.util.PreferencesUtils.getPreferenceUserId;
import static au.com.ourpillstalkpaid.util.PreferencesUtils.setPreference;

public class UserInfoActivity extends AppCompatActivity {

    private ActivityUserInfoBinding binding;
    private MyApplication myApplication;
    private SharedPreferences.Editor edt;
    private SharedPreferences sp;
    private Context ctx;
    private boolean saveInfo;
    private int index1;
    private int index2;
    private String fName;
    private String lName;
    private String gender;
    private String email;
    private String yob;
    private String postcode;
    private String country;
    private String lang;
    private String allergies;
    private String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_info);

        myApplication = (MyApplication) getApplicationContext();
        ctx = getApplicationContext();

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        edt = sp.edit();

        saveInfo = getIntent().getBooleanExtra("saveInfo", true);
        if (saveInfo) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.update_my_details);
        }

        binding.rbMale.setChecked(true);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.country_name_array, android.R.layout.simple_spinner_dropdown_item);

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        binding.spnrCountryOfOrigin.setAdapter(adapter1);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.language_array, android.R.layout.simple_spinner_dropdown_item);

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        binding.spnrPreferedLang.setAdapter(adapter2);

        binding.edtFirstName.setText(MyApplication.firstName);
        binding.edtLastName.setText(MyApplication.lastName);
        binding.edtEmail.setText(MyApplication.emailAddress);
        binding.edtYearOfBirth.setText(MyApplication.yearOfBirth);
        binding.edtPostCode.setText(MyApplication.postCode);

        binding.spnrCountryOfOrigin.setSelection(myApplication.countrySpnrIndex);
        binding.spnrPreferedLang.setSelection(MyApplication.spnrIndex);

        binding.edtAllergies.setText(MyApplication.allergies);
        binding.edtEmergencyContactNumber.setText(MyApplication.emergencyContactNumber);

        if (binding.rbFemale.getText().toString().equals(MyApplication.gender)) {
            binding.rbFemale.setChecked(true);
        } else {
            binding.rbMale.setChecked(true);
        }

        binding.spnrCountryOfOrigin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                index1 = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spnrPreferedLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                index2 = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.btnPrivatePolicy.setOnClickListener(v -> new AlertDialog.Builder(UserInfoActivity.this)
                .setTitle("Privacy Policy")
                .setMessage("When you register for use of the Our Pills Talk App we will collect the data and information outlined below in Schedule 1. \n" +
                        "Your actual identity, namely your name, will not be collected. \n" +
                        "The data and personal information we collect allows us to keep you posted on software updates and our latest product announcements. We also use your personal information to help us create, develop, operate, deliver and improve our product, services, content and advertising and for loss prevention and anti-fraud purposes, and to assist with identification of users and to determine the appropriate services. We may also use personal information for internal purposes such as auditing, data analysis and research to improve our product, services and customer communications. Your email address will only be used in an emergency situation.\n" +
                        "We take the security of your personal information very seriously. The information and data will be stored on a secure database. The information and data supplied by you other than the data referred to in Schedule 2, may be shared with third party entities, such as marketing or pharmaceutical firms. \n" +
                        "Should an arrangement take place with third party entities, direct access will not be provided to the stored data, only exported excerpts closely vetted and screened for any personal information. \n" +
                        "You may at any time request that the data or information supplied is inaccurate or that we delete the data. We may decline to process requests that are frivolous/vexatious, jeopardise the privacy of others, are extremely impractical or for which access is not otherwise required by law. \n" +
                        "We shall use all reasonable efforts to protect information submitted by you in connection with the service provided, but you agree that your submission of such information is at your sole risk and we hereby disclaim any and all liability for any loss or liability relating to such information in any way. \n" +
                        "We also disclaim all liability for any failure of the Our Pills Talk App arising out of any one or more of the following:-(a) The preferred language not being available. (b) The preferred language not translating accurately, due to translations being provided by an external third party. (c) Failure of Pharmacists to administer the QR code. (d) Incompatibility with the operating system used by you or the Pharmacist. (e) Failure by you to upgrade to the latest version of the App. (f) Failure by you to consent to the sending of the relevant data. \n" +
                        "\nSchedule 1.\n" +
                        "Stored Data Attributes: \n- Email Address \n- Year of Birth \n- Post Code \n- Gender \n- Country of Origin \n- Preferred Language \n- Medical History \n- Allergies. \n" +
                        "Data Captured at each Scan: \n- Time of Scan \n- Date of Scan \n- Date Pharmaceuticals Dispensed \n- Pharmacy ID \n- Dosage. \n" +
                        "\nSchedule 2.\n" +
                        "Data not shared to Third Party entities: - Email Address - First Name - Last Name\n")
                .setPositiveButton("Okay", (dialog, which) -> {
                }).show());

        binding.btnSaveDetails.setOnClickListener(v -> {
            if (binding.edtFirstName.getText().toString().isEmpty()) {
                fName = "";
            } else {
                fName = binding.edtFirstName.getText().toString();
            }

            if (binding.edtLastName.getText().toString().isEmpty()) {
                lName = "";
            } else {
                lName = binding.edtLastName.getText().toString();
            }

            if (binding.rbMale.isChecked()) {
                gender = (binding.rbMale.getText().toString());
            } else {
                gender = (binding.rbFemale.getText().toString());
            }

            if (binding.edtEmail.getText().toString().isEmpty()) {
                email = "";
            } else {
                email = binding.edtEmail.getText().toString();
            }

            if (binding.edtYearOfBirth.getText().toString().isEmpty()) {
                binding.tilYearOfBirth.setError("Please Enter your Year Of Birth");
            } else {
                yob = binding.edtYearOfBirth.getText().toString();
            }

            if (binding.edtPostCode.getText().toString().isEmpty()) {
                binding.tilPostCode.setError("Please Enter your Postcode");
            } else {
                binding.tilPostCode.setErrorEnabled(false);
                postcode = binding.edtPostCode.getText().toString();
            }

            country = binding.spnrCountryOfOrigin.getSelectedItem().toString();
            lang = binding.spnrPreferedLang.getSelectedItem().toString();

            if (binding.edtAllergies.getText().toString().isEmpty()) {
                allergies = "";
            } else {
                allergies = binding.edtAllergies.getText().toString();
            }

            if (binding.edtEmergencyContactNumber.getText().toString().isEmpty()) {
                number = "";
            } else {
                number = binding.edtEmergencyContactNumber.getText().toString();
            }

            if (!binding.edtYearOfBirth.getText().toString().isEmpty() && !binding.edtPostCode.getText().toString().isEmpty()) {
                if (binding.edtYearOfBirth.getText().toString().equalsIgnoreCase(" ")) {

                } else if (((Integer.parseInt(binding.edtYearOfBirth.getText().toString().trim())) < 1910
                        || (Integer.parseInt(binding.edtYearOfBirth.getText().toString().trim())) > MyApplication.getYearString())) {
                    binding.tilYearOfBirth.setError("You must enter a valid year greater than 1910 and less than " + MyApplication.getYearString());
                } else if (binding.edtEmail.getText().toString().equalsIgnoreCase("")) {
                    save();
                } else {
                    if (isEmailValid(binding.edtEmail.getText().toString().trim())) {
                        save();
                    } else {
                        binding.tilYearOfBirth.setErrorEnabled(false);
                        binding.tilEmail.setError("Please enter valid email ID");
                        binding.edtEmail.requestFocus();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!saveInfo) {
            setPreference("firstName", " ", ctx);
            setPreference("lastName", " ", ctx);
            setPreference("gender", " ", ctx);
            setPreference("email", " ", ctx);
            setPreference("yob", " ", ctx);
            setPreference("postcode", " ", ctx);
            setPreference("country", " ", ctx);
            setPreference("country", " ", ctx);
            setPreference("lang", " ", ctx);
            setPreference("allergies", " ", ctx);
            setPreference("number", " ", ctx);
            edt.putBoolean("SaveInfo", true);
            edt.commit();
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void save() {
        String languageCode = getResources().getStringArray(R.array.language_code_array)[index2];

        setPreference("CountrySpnrIndex", index1 + "", getApplicationContext());
        setPreference("Index", index2 + "", getApplicationContext());
        setPreference("Lang", languageCode, getApplicationContext());
        setPreference("myLang", languageCode, getApplicationContext());
        setPreference("firstName", fName, ctx);
        setPreference("lastName", lName, ctx);
        setPreference("gender", gender, ctx);
        setPreference("email", email, ctx);
        setPreference("yob", yob, ctx);
        setPreference("postcode", postcode, ctx);
        setPreference("country", country, ctx);
        setPreference("lang", lang, ctx);
        setPreference("allergies", allergies, ctx);
        setPreference("number", number, ctx);

        myApplication.countrySpnrIndex = index1;
        MyApplication.spnrIndex = index2;
        MyApplication.lang = languageCode;
        MyApplication.firstName = fName;
        MyApplication.lastName = lName;
        MyApplication.gender = gender;
        MyApplication.emailAddress = email;
        MyApplication.yearOfBirth = yob;
        MyApplication.postCode = postcode;
        MyApplication.countryOfOrigin = country;
        MyApplication.preferedLang = lang;
        MyApplication.allergies = allergies;
        MyApplication.emergencyContactNumber = number;

        Map<String, String> mDetails = new HashMap<>();
        mDetails.put("email", email);
        mDetails.put("year_of_birth", yob);
        mDetails.put("postcode", postcode);
        mDetails.put("gender", gender);
        mDetails.put("country_of_origin", country);
        mDetails.put("language", lang);
        mDetails.put("allergies", allergies);
        mDetails.put("userID", getPreferenceUserId(this));

        if (Utils.hasConnection(this)) {
            webServicePart(mDetails);
        } else {
            edt.putBoolean("SaveInfo", true);
            edt.putBoolean("saveSpinnerIndex", true);
            edt.putBoolean("saveCountrySpinnerIndex", true);
            edt.commit();
        }
    }

    private void webServicePart(Map<String, String> dataParam) {
        String url = Config.URL_INSERT_USER;
        Log.d("Inside Webservice", dataParam.toString());

        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, url, dataParam, response -> {
            try {
                if (response != null) {
                    Log.d("Response: ", response.toString());

                    String id = response.getString("userid");
                    setPreference(USER_ID, id, ctx);

                    edt.putBoolean("SaveInfo", true);
                    edt.putBoolean("saveSpinnerIndex", true);
                    edt.putBoolean("saveCountrySpinnerIndex", true);
                    edt.commit();

                    Toast.makeText(UserInfoActivity.this, R.string.user_info_saved_success, Toast.LENGTH_SHORT).show();

                    setResult(RESULT_OK);
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, error -> {
            NetworkResponse response = error.networkResponse;
            if (error instanceof ServerError && response != null) {
                try {
                    String res = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                    Log.e("error body", res);
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }

            Toast.makeText(UserInfoActivity.this, R.string.user_info_saved_failed, Toast.LENGTH_SHORT).show();
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonReq);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activity = null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }

}
