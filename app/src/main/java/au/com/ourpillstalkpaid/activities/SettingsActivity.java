package au.com.ourpillstalkpaid.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import au.com.ourpillstalkpaid.Config;
import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.ActivitySettingsBinding;
import au.com.ourpillstalkpaid.model.MonthName;
import au.com.ourpillstalkpaid.model.Translation;
import au.com.ourpillstalkpaid.services.TextToSpeechService;
import au.com.ourpillstalkpaid.services.TranslationService;
import au.com.ourpillstalkpaid.services.dto.LangTrans;
import au.com.ourpillstalkpaid.util.StringUtils;
import au.com.ourpillstalkpaid.util.Utils;

import static au.com.ourpillstalkpaid.util.PreferencesUtils.setPreference;

public class SettingsActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private static final String TAG = SettingsActivity.class.getSimpleName();

    private ActivitySettingsBinding binding;
    private SharedPreferences sp;
    private SharedPreferences.Editor edt;
    private String languageCode;
    private boolean ignoreReloadLangAtFirst = true;

    private TextToSpeechService ttsService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ttsService = TextToSpeechService.getInstance(this, this);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        edt = sp.edit();

        binding.rgSpeech.setOnCheckedChangeListener((group, checkedId) ->
                edt.putBoolean("speech_alternative_" + languageCode, checkedId == R.id.rb_speech_alternative).apply());
        binding.btReloadLang.setOnClickListener(v ->
                executeGetLangTransData());
        binding.ivReloadLangInfo.setOnClickListener(v ->
                Toast.makeText(this, binding.ivReloadLangInfo.getTag().toString(), Toast.LENGTH_SHORT).show());

        binding.llMyDetails.setOnClickListener(v ->
                startActivity(new Intent(SettingsActivity.this, UserInfoActivity.class)));
        binding.llHealth.setOnClickListener(v -> {
            Uri uri = Uri.parse(Config.HEALTH_INFO_URL);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(myIntent);
        });
        binding.llHelp.setOnClickListener(v ->
                startActivity(new Intent(SettingsActivity.this, HelpActivity.class)));
    }

    @Override
    public void onInit(int status) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.language_array, R.layout.item_language);
        binding.spLanguageChooser.setAdapter(adapter);
        binding.spLanguageChooser.setSelection(MyApplication.spnrIndex);
        binding.spLanguageChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                languageCode = getResources().getStringArray(R.array.language_code_array)[position];

                setPreference("Index", position + "", getApplicationContext());
                setPreference("Lang", languageCode, getApplicationContext());
                setPreference("myLang", languageCode, getApplicationContext());
                edt.putBoolean("saveSpinnerIndex", true);
                edt.commit();
                MyApplication.spnrIndex = position;
                MyApplication.lang = languageCode;
                reloadTranslatedViews();

                updateSpeechProviderUI(languageCode);

                if (ignoreReloadLangAtFirst) {
                    ignoreReloadLangAtFirst = false;
                } else {
                    executeGetLangTransData();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void reloadTranslatedViews() {
        if (Utils.hasConnection(this)) {
            binding.tvMyDetailsTranslated.setVisibility(View.VISIBLE);
            binding.tvHealthTranslated.setVisibility(View.VISIBLE);
            binding.tvHelpTranslated.setVisibility(View.VISIBLE);
            binding.txtTranslatedselectLanguage.setVisibility(View.VISIBLE);

            if (MyApplication.lang.equalsIgnoreCase(Constants.EN)) {
                binding.tvMyDetailsTranslated.setVisibility(View.GONE);
                binding.tvHealthTranslated.setVisibility(View.GONE);
                binding.tvHelpTranslated.setVisibility(View.GONE);
                binding.txtTranslatedselectLanguage.setVisibility(View.GONE);
            }

            List<String> texts = new ArrayList<>();
            texts.add(getString(R.string.select_language));
            texts.add(getString(R.string.my_details));
            texts.add(getString(R.string.health_info));
            texts.add(getString(R.string.help));
            texts.add(getString(R.string.speech_provider));
            texts.add(getString(R.string.speech_is_not_available_for_this_language));
            texts.add(getString(R.string.default_));
            texts.add(getString(R.string.alternative));
            texts.add(getString(R.string.reload_language));
            texts.add(getString(R.string.download_the_latest_translation_file));
            texts.add(getString(R.string.notification_alarm_refill));
            texts.add(getString(R.string.notification_alarm_take));
            texts.add(TextUtils.join("|", Constants.MONTH_NAMES));

            List<View> views = new ArrayList<>();
            views.add(binding.txtSelectLanguage);
            views.add(binding.tvMyDetails);
            views.add(binding.tvHealth);
            views.add(binding.tvHelp);
            views.add(binding.tvSpeechProvider);
            views.add(binding.tvSpeechNotAvailable);
            views.add(binding.rbSpeechDefault);
            views.add(binding.rbSpeechAlternative);
            views.add(binding.tvReloadLang);
            views.add(binding.ivReloadLangInfo);

            new TranslationService(this, texts, views, true) {
                @Override
                protected void onPostExecute(String[] translatedTexts) {
                    super.onPostExecute(translatedTexts);

                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("notification_alarm_refill", translatedTexts[translatedTexts.length - 3]);
                    editor.putString("notification_alarm_take", translatedTexts[translatedTexts.length - 2]);
                    editor.apply();

                    // save month names data
                    new Delete().from(MonthName.class).execute();
                    if (!MyApplication.lang.equalsIgnoreCase(Constants.EN)) {
                        String monthNames = translatedTexts[translatedTexts.length - 1];
                        if (monthNames != null) {
                            String[] names = monthNames.split("\\|");
                            for (String name : names) {
                                new MonthName(StringUtils.capitalizeFirstLetter(name.trim())).save();
                            }
                        }
                    }
                }
            }.execute();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
        ttsService.shutdown();
        startActivity(new Intent(SettingsActivity.this, MainActivity.class));
    }

    private void updateSpeechProviderUI(String languageCode) {
        binding.llReloadLang.setVisibility(Constants.EN.equalsIgnoreCase(languageCode) ? View.GONE : View.VISIBLE);

        int state = ttsService.checkSpeechProviderAvailable(languageCode);
        binding.llSpeechModeNone.setVisibility(state == 0 ? View.VISIBLE : View.GONE);
        binding.llSpeechModeBoth.setVisibility(state == 1 ? View.VISIBLE : View.GONE);
        if (state == 1) {
            boolean useAlternative = sp.getBoolean("speech_alternative_" + languageCode, false);
            binding.rbSpeechDefault.setChecked(!useAlternative);
            binding.rbSpeechAlternative.setChecked(useAlternative);
        }
    }

    private void executeGetLangTransData() {
        if (Constants.EN.equalsIgnoreCase(languageCode)) return;

        String langName = binding.spLanguageChooser.getSelectedItem().toString();
        String url = Config.

                LANG_TRANS_DATA_URL + URLEncoder.encode(langName);
        Log.d(TAG, url);

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.reload_language));
        dialog.show();

        StringRequest request = new StringRequest(url, response -> {
            Log.d(TAG, "Response: " + response);

            // clear translation data
            new Delete().from(Translation.class).execute();

            try {
                Serializer serializer = new Persister();
                LangTrans langTrans = serializer.read(LangTrans.class, response);

                // save translation data
                for (Translation translation : langTrans.getFullTranslations()) {
                    translation.setFull(true);
                    translation.save();
                }
                for (Translation translation : langTrans.getPartialTranslations()) {
                    translation.save();
                }

                // update ui
                binding.btReloadLang.setImageResource(R.drawable.done);
                binding.btReloadLang.setBackgroundColor(ContextCompat.getColor(this, R.color.gray));
                binding.btReloadLang.setEnabled(false);
            } catch (Exception e) {
                e.printStackTrace();
            }

            dialog.dismiss();
        }, error -> dialog.dismiss()) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        Volley.newRequestQueue(this).add(request);
    }
    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activity=null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity=this;
    }

}
