package au.com.ourpillstalkpaid.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import au.com.ourpillstalkpaid.BuildConfig;
import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.ActivityMainBinding;
import au.com.ourpillstalkpaid.services.TranslationService;
import au.com.ourpillstalkpaid.util.Utils;

import static au.com.ourpillstalkpaid.util.PreferencesUtils.getPreferenceUserId;

public class MainActivity extends AppCompatActivity {

    private String myLang;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        myLang = MyApplication.lang;

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_home_title_icon);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        getSupportActionBar().setSubtitle("v" + BuildConfig.VERSION_NAME);

        binding.btnScanner.setOnClickListener(v -> {
            if ("0".equalsIgnoreCase(getPreferenceUserId(this))) {
                Toast.makeText(MainActivity.this, "Please Update the info first", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, UserInfoActivity.class));
            } else {
                startActivity(new Intent(MainActivity.this, ScanActivity.class));
            }
        });

        binding.btnMyDetails.setOnClickListener(v ->
                startActivity(new Intent(MainActivity.this, MyDetailsActivity.class)));

        binding.btnSettings.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            finish();
        });

        binding.btnMedicine.setOnClickListener(v ->
                startActivity(new Intent(MainActivity.this, MedicineActivity.class)));

        binding.btAlarms.setOnClickListener(v ->
                startActivity(new Intent(MainActivity.this, AlarmsActivity.class)));

        // request update info if not filled yet
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!sp.getBoolean("SaveInfo", false)) {
            Intent i = new Intent(MainActivity.this, UserInfoActivity.class);
            i.putExtra("saveInfo", false);
            startActivity(i);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        translateUI();
        MyApplication.activity=this;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activity=null;

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        translateUI();
    }

    private void translateUI() {
        if (!myLang.equalsIgnoreCase(Constants.EN)) {
            if (Utils.hasConnection(this)) {
                List<View> views = new ArrayList<>();
                views.add(binding.txtScanText);
                views.add(binding.txtMyDetails);
                views.add(binding.txtSettingText);
                views.add(binding.txtMedicine);
                views.add(binding.tvAlarms);
                TranslationService.translateViews(views, this);

                binding.txtTranslatedMyDetails.setVisibility(View.VISIBLE);
                binding.txtTranslatedMedicine.setVisibility(View.VISIBLE);
                binding.txtTranslatedsettingText.setVisibility(View.VISIBLE);
                binding.txtTranslatedscanText.setVisibility(View.VISIBLE);
                binding.tvAlarmsEn.setVisibility(View.VISIBLE);
            } else {
                myLang = Constants.EN;
                binding.txtTranslatedMyDetails.setVisibility(View.GONE);
                binding.txtTranslatedMedicine.setVisibility(View.GONE);
                binding.txtTranslatedsettingText.setVisibility(View.GONE);
                binding.txtTranslatedscanText.setVisibility(View.GONE);
                binding.tvAlarmsEn.setVisibility(View.GONE);
            }
        } else {
            binding.txtTranslatedMyDetails.setVisibility(View.GONE);
            binding.txtTranslatedMedicine.setVisibility(View.GONE);
            binding.txtTranslatedsettingText.setVisibility(View.GONE);
            binding.txtTranslatedscanText.setVisibility(View.GONE);
            binding.tvAlarmsEn.setVisibility(View.GONE);
        }
    }
}
