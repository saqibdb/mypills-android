package au.com.ourpillstalkpaid.activities;

import android.app.Fragment;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.ActivityMyDetailsBinding;
import au.com.ourpillstalkpaid.fragments.UserInfoFragment;

public class MyDetailsActivity extends AppCompatActivity {

    private static final int REQUEST_UPDATE_INFO = 1;
    private ActivityMyDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_details);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViewById(R.id.tv_edit).setOnClickListener(view ->
                startActivityForResult(new Intent(MyDetailsActivity.this, UserInfoActivity.class), REQUEST_UPDATE_INFO));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_UPDATE_INFO && resultCode == RESULT_OK) {
            Fragment fragment = getFragmentManager().findFragmentById(R.id.frg_user_info);
            if (fragment instanceof UserInfoFragment) {
                ((UserInfoFragment) fragment).loadUserInfo();
            }
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activity=null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity=this;
    }

}
