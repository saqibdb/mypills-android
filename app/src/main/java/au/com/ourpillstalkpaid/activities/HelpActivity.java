package au.com.ourpillstalkpaid.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Collections;

import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.ActivityHelpBinding;
import au.com.ourpillstalkpaid.services.TextToSpeechService;
import au.com.ourpillstalkpaid.services.TranslationService;
import au.com.ourpillstalkpaid.util.Utils;

public class HelpActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private ActivityHelpBinding binding;
    private TextToSpeechService ttsService;
    private String myLang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_help);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myLang = MyApplication.lang;
        translateUI();

        ttsService = TextToSpeechService.getInstance(this, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            ttsService.resetQueue();
            boolean available = ttsService.speak(binding.txtHelpText.getText().toString(), myLang);
            if (!available) {
                binding.tvSpeechNotAvailable.setVisibility(View.VISIBLE);
            }
        } else {
            Toast.makeText(this, R.string.tts_can_not_started, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ttsService.stopSpeak();
        MyApplication.activity=null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        translateUI();
        MyApplication.activity=this;

    }

    @Override
    public void finish() {
        super.finish();
        // do not shutdown tts service here
    }

    private void translateUI() {
        if (!myLang.equalsIgnoreCase(Constants.EN)) {
            if (Utils.hasConnection(this)) {
                TranslationService.translateViews(Collections.singletonList(binding.txtHelpText), this);
            } else {
                myLang = Constants.EN;
            }
        }
    }
}
