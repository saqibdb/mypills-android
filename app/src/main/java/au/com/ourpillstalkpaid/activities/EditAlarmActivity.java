package au.com.ourpillstalkpaid.activities;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.activeandroid.query.Select;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import au.com.ourpillstalkpaid.BR;
import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.ActivityAlarmEditBinding;
import au.com.ourpillstalkpaid.model.Alarm;
import au.com.ourpillstalkpaid.model.Translation;
import au.com.ourpillstalkpaid.services.TranslationService;
import au.com.ourpillstalkpaid.util.AlarmUtils;
import au.com.ourpillstalkpaid.util.DialogUtils;

import static au.com.ourpillstalkpaid.model.Alarm.REFILL_PRESCRIPTION;

public class EditAlarmActivity extends AppCompatActivity {

    private ActivityAlarmEditBinding binding;
    private Alarm alarm;
    private String description;
    private boolean fullTranslated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_alarm_edit);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        alarm = getIntent().getParcelableExtra("Alarm");

        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("StaticFieldLeak")
    private void initUI() {
        binding.setVariable(BR.alarm, alarm);
        description = alarm.getDescription();
        if (!MyApplication.lang.equalsIgnoreCase(Constants.EN)) {
            // run full translating
            List<String> texts = Arrays.asList(description);
            fullTranslated = Translation.fullTranslation(texts);
            if (fullTranslated) {
                description = texts.get(0);
                binding.setVariable(BR.description, description);
            } else {
                new TranslationService(this, texts, true) {
                    @Override
                    protected void onPostExecute(String[] translatedTexts) {
                        super.onPostExecute(translatedTexts);
                        // run partial translating
                        List<String> result = Arrays.asList(translatedTexts[0]);
                        Translation.partialTranslation(result);
                        description = result.get(0);

                        binding.setVariable(BR.description, description);
                    }
                }.execute();
            }
        } else {
            binding.setVariable(BR.description, description);
        }

        binding.ivEdit.setOnClickListener(v -> {
            if (alarm.getMode() == REFILL_PRESCRIPTION) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(alarm.getDateTime());

                DialogUtils.showDatePickerDialog(this, calendar, (view, year, month, dayOfMonth) -> {
                    alarm.setDateTime(new GregorianCalendar(year, month, dayOfMonth).getTime());
                    binding.setVariable(BR.alarm, alarm);
                });
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(alarm.getDateTime());

                DialogUtils.showTimePickerDialog(this, calendar, (view, hourOfDay, minute) -> {
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    calendar.set(Calendar.MINUTE, minute);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);

                    // check duplicated
                    Date oldTime = alarm.getDateTime();
                    alarm.setDateTime(calendar.getTime());
                    List<Alarm> takeAlarms = new Select().from(Alarm.class).where("scannedPillId = ?", alarm.getScannedPillId()).execute();
                    for (Alarm al : takeAlarms) {
                        if (!al.getId().equals(alarm.getId()) && al.formatDateTime().equals(alarm.formatDateTime())) {
                            alarm.setDateTime(oldTime);
                            Toast.makeText(this, R.string.duplicated_alarm, Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    binding.setVariable(BR.alarm, alarm);
                });
            }
        });

        binding.ivSave.setOnClickListener(v -> {
            AlarmUtils.cancelAlarm(this, alarm.getId());
            AlarmUtils.setAlarm(this, alarm);
            alarm.save();

            Toast.makeText(this, R.string.saved, Toast.LENGTH_SHORT).show();
            setResult(RESULT_OK);
            finish();
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activity = null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
    }

}
