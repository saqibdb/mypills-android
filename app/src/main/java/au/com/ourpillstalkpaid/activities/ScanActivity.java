package au.com.ourpillstalkpaid.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.ourpillstalkpaid.Config;
import au.com.ourpillstalkpaid.Constants;
import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.database.DBHelper;
import au.com.ourpillstalkpaid.databinding.ActivityScanBinding;
import au.com.ourpillstalkpaid.model.Alarm;
import au.com.ourpillstalkpaid.model.ScannedPill;
import au.com.ourpillstalkpaid.model.Translation;
import au.com.ourpillstalkpaid.services.CustomRequest;
import au.com.ourpillstalkpaid.services.TextToSpeechService;
import au.com.ourpillstalkpaid.services.TranslationService;
import au.com.ourpillstalkpaid.util.StringUtils;
import au.com.ourpillstalkpaid.util.Utils;

import static au.com.ourpillstalkpaid.util.PreferencesUtils.getPreferenceUserId;

public class ScanActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private static final String TAG = ScanActivity.class.getSimpleName();

    private ActivityScanBinding binding;
    private ScannedPill scannedPill;
    private String selectedLang;

    private static final String PRESCRIPTION_TAG = "p";
    private static final String SCAN_NUM_TAG = "scan_num";
    private static final String DATE_TAG = "date";
    private static final String PAT_NAME_TAG = "n";
    private static final String DRUG_NAME_TAG = "dg";
    private static final String EXP_INSTRUC_TAG = "in";
    private static final String SCRIPT_ID_TAG = "id";
    private static final String PHARM_NAME_TAG = "pm";
    private static final String DATE_DISP_TAG = "dt";
    private static final String CMI_NUM_TAG = "cn";
    private static final String DATE_FRMT_TAG = "df";

    private TextToSpeechService ttsService;
    private String info1, info2, info2Speak, info3;
    private boolean fullTranslated;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_scan);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectedLang = MyApplication.lang;

        ttsService = TextToSpeechService.getInstance(this, this);

        scannedPill = getIntent().getParcelableExtra("ScannedPill");
        if (scannedPill == null) {
            String placeHolder = getString(R.string.scan_placeholder);
            if (!selectedLang.equalsIgnoreCase(Constants.EN)) {
                new TranslationService(this, Collections.singletonList(placeHolder), false) {
                    @Override
                    protected void onPostExecute(String[] translatedTexts) {
                        super.onPostExecute(translatedTexts);
                        startScan(translatedTexts[0]);
                    }
                }.execute();
            } else {
                startScan(placeHolder);
            }
        }

        binding.ivDelete.setOnClickListener(v -> {
            DBHelper.deleteAlarmsOfPill(this, scannedPill).delete();

            Toast.makeText(this, "Delete Perform", Toast.LENGTH_SHORT).show();
            setResult(RESULT_OK);
            finish();
        });

        binding.fabReplay.setOnClickListener(v -> speakText());

        binding.fabStop.setOnClickListener(v -> ttsService.stopSpeak());

        binding.fabCmi.setOnClickListener(v -> new AlertDialog.Builder(ScanActivity.this)
                .setTitle("Leaving Our Pills Talk")
                .setMessage("CMI is an external website and is not affiliated with Our Pills Talk.\n\nLeave Our Pills Talk to view CMI for " + scannedPill.getNameSub() + "?")
                .setPositiveButton("OK", (dialog, which) -> {
                    Intent i = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://www.medicines.org.au/mob/consumer-results.cfm?searchtext=" + scannedPill.getNameSub()));
                    startActivity(i);
                }).
                        setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss()).
                        show());

        binding.llAlarm.setOnClickListener(v -> {
            Intent intent = new Intent(ScanActivity.this, AlarmsActivity.class);
            intent.putExtra("ScannedPill", scannedPill);
            startActivity(intent);
        });
    }

    @Override
    public void onInit(int status) {
        if (status != TextToSpeech.SUCCESS) {
            Toast.makeText(this, R.string.tts_can_not_started, Toast.LENGTH_LONG).show();
        }

        if (scannedPill != null) {
            updateScannedInfoUI(scannedPill);
            translateAndSpeak();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity = this;
        if (scannedPill != null) {
            int count = new Select().from(Alarm.class).where("scannedPillId = ?", scannedPill.getId()).count();
            binding.tvAlarmsCount.setText("(" + count + ")");
        }
    }

    private void startScan(String promptMessage) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(CustomCaptureActivity.class);
        integrator.setPrompt(promptMessage);
        integrator.initiateScan();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ttsService.stopSpeak();
        MyApplication.activity = null;
    }

    @Override
    public void finish() {
        super.finish();
        ttsService.shutdown();
        MyApplication.activity = null;

    }

    private void speakText() {
        ttsService.resetQueue();

        if (!ttsService.speak(info1, Constants.EN)) {
            binding.tvSpeechNotAvailable.setVisibility(View.VISIBLE);
        }

        if (!ttsService.speak(info2Speak, selectedLang)) {
            binding.tvSpeechNotAvailable.setVisibility(View.VISIBLE);
        }

        String info3Speak = info3;
        if (Constants.EN.equalsIgnoreCase(selectedLang)) {
            info3Speak = Constants.DISPENSED + " " + info3Speak;
        }
        if (!ttsService.speak(info3Speak, selectedLang)) {
            binding.tvSpeechNotAvailable.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                finish();
            } else {
                handleScanResult(result.getContents());
            }
        }
    }

    private void handleScanResult(String scanResult) {
        scanResult = scanResult.replace("&", "and");
        Log.i("scanresult", scanResult);
                /*ToneGenerator tone = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                tone.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 400);*/

        Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);

        if (isPrescriptionScanXML(scanResult)) {
            // ignores namespace in p tag
            scanResult = "<" + PRESCRIPTION_TAG + scanResult.substring(scanResult.indexOf(">"));

            scanResult = MyApplication.abbreviations(scanResult);
            scanResult = MyApplication.abbreviationsTwo(scanResult);

            HashMap<String, String> parseXml = getXMLQRScanMap(scanResult);
            String xml = getScanInfoToDisplay(parseXml);

            String name = StringUtils.trim(parseXml.get(DRUG_NAME_TAG));
            String pharmacyName = StringUtils.trim(parseXml.get(PHARM_NAME_TAG));
            String dispensedDate = StringUtils.trim(parseXml.get(DATE_DISP_TAG));
            String dateFormat = StringUtils.trim(parseXml.get(DATE_FRMT_TAG));

            String dispensedDate_ddmmyyyy = getDate_ddmmyyyy(dispensedDate, dateFormat);
            String instruction = parseXml.get(EXP_INSTRUC_TAG).trim();
            scannedPill = DBHelper.findOne(name, instruction);

            Map<String, String> insertScanParams = new HashMap<>();
            insertScanParams.put("userID", getPreferenceUserId(this));
            insertScanParams.put("date_format", TextUtils.isEmpty(dateFormat) ? "" : dateFormat);
            insertScanParams.put("dispense_date", dispensedDate);
            insertScanParams.put("pharmaceutical", name);
            insertScanParams.put("pharmacy_number", pharmacyName);

            if (scannedPill != null) {
                // if SavedScan.DispensedDate != ThisScan.DispensedDate then
                //     if a Refill Prescription Alarm exists for this drugname & instruction
                //         delete the Refill Prescription Alarm
                if (!dispensedDate_ddmmyyyy.equals(scannedPill.getDispensedDate())) {
                    DBHelper.deleteAlarmsOfPill(this, scannedPill, Alarm.REFILL_PRESCRIPTION);

                    callPost(insertScanParams);
                }

                // update
                scannedPill.setDispensedDate(dispensedDate_ddmmyyyy);
                scannedPill.setUpdatedDate();

                // update time format
                String oldXml = scannedPill.getXml();
                xml = oldXml.substring(0, oldXml.lastIndexOf(Constants.DISPENSED)) + xml.substring(xml.lastIndexOf(Constants.DISPENSED));
                scannedPill.setXml(xml);
                scannedPill.save();
            } else {
                // insert
                scannedPill = new ScannedPill(xml, name, pharmacyName, dispensedDate_ddmmyyyy);
                scannedPill.save();
                DBHelper.deleteOldScans(this, Config.MAX_SCAN_ITEMS);

                callPost(insertScanParams);
            }

            updateScannedInfoUI(scannedPill);
            translateAndSpeak();

            Log.d("lastScanTextView", binding.lastScanTextView.getText().toString());
        } else {
            binding.ivDelete.setVisibility(View.INVISIBLE);
            binding.lastScanTextView.setVisibility(View.VISIBLE);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void updateScannedInfoUI(ScannedPill scannedPill) {
        String[] items = scannedPill.getXml().replace("\n\n", "\n").split("\n");
        info1 = items[0].trim() + "\n" + items[1].trim();
        info2 = scannedPill.getInstructions();
        info2Speak = MyApplication.abbreviationsUnitsToSpeak(info2);

        binding.tvDrug.setText(info1.replace("/", " per "));
        binding.tvPillsInfo.setText(info2);

        binding.lastScanTextView.setVisibility(View.GONE);
        info3 = scannedPill.formatDispensedDate();

        if (!selectedLang.equalsIgnoreCase(Constants.EN)) {
            List<String> texts = new ArrayList<>();
            texts.add(info3);
            new TranslationService(this, texts, true) {
                @Override
                protected void onPostExecute(String[] translatedTexts) {
                    super.onPostExecute(translatedTexts);
                    if (!fullTranslated) {
                        // run partial translating
                        List<String> result = Arrays.asList(translatedTexts[0]);
                        Translation.partialTranslation(result);
                        info3 = result.get(0);
                        binding.tvDispensedDate.setText(Constants.DISPENSED + " " + info3);
                    }
                }
            }.execute();
        }else {
            binding.tvDispensedDate.setText(Constants.DISPENSED + " " + info3);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void translateAndSpeak() {
        if (!selectedLang.equalsIgnoreCase(Constants.EN)) {
            // run full translating
            List<String> texts = new ArrayList<>();
            texts.add(info2);
            texts.add(info2Speak);
            fullTranslated = Translation.fullTranslation(texts);
            if (fullTranslated) {
                info2 = texts.get(0);
                info2Speak = texts.get(1);
                binding.tvPillsInfo.setText(info2);
            }

            texts.add(binding.lastScanTextView.getText().toString());
            new TranslationService(this, texts, true) {
                @Override
                protected void onPostExecute(String[] translatedTexts) {
                    super.onPostExecute(translatedTexts);
                    binding.lastScanTextView.setText(translatedTexts[2]);

                    if (!fullTranslated) {
                        // run partial translating
                        List<String> result = Arrays.asList(translatedTexts[0], translatedTexts[1]);
                        Translation.partialTranslation(result);
                        info2 = result.get(0);
                        info2Speak = result.get(1);
                        binding.tvPillsInfo.setText(info2);
                    }

                    speakText();
                }
            }.execute();
        } else {
            speakText();
        }
    }

    @SuppressLint("SimpleDateFormat")
    private String getDate_ddmmyyyy(String dispensedDate, String formatDate) {
        if ("md".equals(formatDate)) {
            SimpleDateFormat mdformat = new SimpleDateFormat("M/d/yyyy");
            SimpleDateFormat dmformat = new SimpleDateFormat(ScannedPill.DISPENSED_DATE_FORMAT);
            try {
                dispensedDate = dmformat.format(mdformat.parse(dispensedDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return dispensedDate;
    }

    private boolean isPrescriptionScanXML(String fileBody) {
        return fileBody.startsWith("<" + PRESCRIPTION_TAG);
    }

    private HashMap<String, String> getXMLQRScanMap(String xml) {
        HashMap<String, String> prescriptionHashMap = new HashMap<>();
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(xml));
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() == XmlPullParser.START_TAG) {
                    if (parser.getName().equals(SCAN_NUM_TAG)) {
                        prescriptionHashMap.put(SCAN_NUM_TAG, parser.nextText());
                    } else if (parser.getName().equals(DATE_TAG)) {
                        prescriptionHashMap.put(DATE_TAG, parser.nextText());
                    } else if (parser.getName().equals(PAT_NAME_TAG)) {
                        prescriptionHashMap.put(PAT_NAME_TAG, parser.nextText());
                    } else if (parser.getName().equals(DRUG_NAME_TAG)) {
                        prescriptionHashMap.put(DRUG_NAME_TAG, parser.nextText());
                    } else if (parser.getName().equals(EXP_INSTRUC_TAG)) {
                        prescriptionHashMap.put(EXP_INSTRUC_TAG, parser.nextText());
                    } else if (parser.getName().equals(SCRIPT_ID_TAG)) {
                        prescriptionHashMap.put(SCRIPT_ID_TAG, parser.nextText());
                    } else if (parser.getName().equals(PHARM_NAME_TAG)) {
                        prescriptionHashMap.put(PHARM_NAME_TAG, parser.nextText());
                    } else if (parser.getName().equals(DATE_DISP_TAG)) {
                        prescriptionHashMap.put(DATE_DISP_TAG, parser.nextText());
                    } else if (parser.getName().equals(CMI_NUM_TAG)) {
                        prescriptionHashMap.put(CMI_NUM_TAG, parser.nextText());
                    } else if (parser.getName().equals(DATE_FRMT_TAG)) {
                        prescriptionHashMap.put(DATE_FRMT_TAG, parser.nextText());
                    }
                }
                parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prescriptionHashMap;
    }

    private String getScanInfoToDisplay(HashMap<String, String> parseXml) {
        String date = "";
        if (parseXml.containsKey(DATE_DISP_TAG)) {
            date = parseXml.get(DATE_DISP_TAG);
            String dateFormat = parseXml.containsKey(DATE_FRMT_TAG) ? StringUtils.trim(parseXml.get(DATE_FRMT_TAG)) : "";
            date = getDate_ddmmyyyy(date, dateFormat);
        } else {
            date = "(no date provided)";
        }

        if (parseXml.containsKey(PHARM_NAME_TAG)) {
            if (parseXml.get(PHARM_NAME_TAG).trim().isEmpty()) {
                if (date.equalsIgnoreCase("(no date provided)")) {
                    Log.e("Data to SHow 1", parseXml.get(DRUG_NAME_TAG) + ":" + parseXml.get(PAT_NAME_TAG) + ":" + parseXml.get(EXP_INSTRUC_TAG) + Constants.DISPENSED + " " + date + "Pharmacy: " + parseXml.get(PHARM_NAME_TAG));

                    return parseXml.get(DRUG_NAME_TAG) + " \r\n\n" + parseXml.get(PAT_NAME_TAG) + ".\n" + parseXml.get(EXP_INSTRUC_TAG);
                } else {
                    Log.e("Data to SHow 2", parseXml.get(DRUG_NAME_TAG) + ":" + parseXml.get(PAT_NAME_TAG) + ":" + parseXml.get(EXP_INSTRUC_TAG) + Constants.DISPENSED + " " + date + "Pharmacy: " + parseXml.get(PHARM_NAME_TAG));

                    return parseXml.get(DRUG_NAME_TAG) + " \r\n\n" + parseXml.get(PAT_NAME_TAG) + ".\n" + parseXml.get(EXP_INSTRUC_TAG) + "\n\n" + Constants.DISPENSED + " " + date;
                }
            } else {
                if (date.equalsIgnoreCase("(no date provided)")) {
                    Log.e("Data to SHow 3", parseXml.get(DRUG_NAME_TAG) + ":" + parseXml.get(PAT_NAME_TAG) + ":" + parseXml.get(EXP_INSTRUC_TAG) + Constants.DISPENSED + " " + date + "Pharmacy: " + parseXml.get(PHARM_NAME_TAG));
                    return parseXml.get(DRUG_NAME_TAG) + " \r\n\n" + parseXml.get(PAT_NAME_TAG) + ".\n" + parseXml.get(EXP_INSTRUC_TAG);// + "\n\nPharmacy: " + parseXml.get(PHARM_NAME_TAG);
                } else {
                    Log.e("Data to SHow 4", parseXml.get(DRUG_NAME_TAG) + ":" + parseXml.get(PAT_NAME_TAG) + ":" + parseXml.get(EXP_INSTRUC_TAG) + Constants.DISPENSED + " " + date + "Pharmacy: " + parseXml.get(PHARM_NAME_TAG));
                    return parseXml.get(DRUG_NAME_TAG) + " \r\n\n" + parseXml.get(PAT_NAME_TAG) + ".\n" + parseXml.get(EXP_INSTRUC_TAG) + "\n\n" + Constants.DISPENSED + " " + date;// + "\nPharmacy: " + parseXml.get(PHARM_NAME_TAG);
                }
            }
        } else {
            if (date.equalsIgnoreCase("(no date provided)")) {
                Log.e("Data to SHow 1", parseXml.get(DRUG_NAME_TAG) + ":" + parseXml.get(PAT_NAME_TAG) + ":" + parseXml.get(EXP_INSTRUC_TAG) + Constants.DISPENSED + " " + date + "Pharmacy: " + parseXml.get(PHARM_NAME_TAG));

                return parseXml.get(DRUG_NAME_TAG) + " \r\n\n" + parseXml.get(PAT_NAME_TAG) + ".\n" + parseXml.get(EXP_INSTRUC_TAG);
            } else {
                Log.e("Data to SHow 2", parseXml.get(DRUG_NAME_TAG) + ":" + parseXml.get(PAT_NAME_TAG) + ":" + parseXml.get(EXP_INSTRUC_TAG) + Constants.DISPENSED + " " + date + "Pharmacy: " + parseXml.get(PHARM_NAME_TAG));

                return parseXml.get(DRUG_NAME_TAG) + " \r\n\n" + parseXml.get(PAT_NAME_TAG) + ".\n" + parseXml.get(EXP_INSTRUC_TAG) + "\n\n" + Constants.DISPENSED + " " + date;
            }
        }
    }

    private void callPost(Map<String, String> dataParam) {
        if (!Utils.hasConnection(this)) return;

        String url = Config.URL_INSERT_SCAN;
        Log.d(TAG, url + "*****" + dataParam.toString());

        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, url, dataParam, response -> {
            try {
                if (response != null) {
                    Log.d("Response: ", response.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, error -> {
            NetworkResponse response = error.networkResponse;
            if (error instanceof ServerError && response != null) {
                try {
                    String res = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                    Log.e("error body", res);
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonReq);
    }

}
