package au.com.ourpillstalkpaid.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.adapter.ScannedPillAdapter;
import au.com.ourpillstalkpaid.database.DBHelper;
import au.com.ourpillstalkpaid.databinding.ActivityMedicineBinding;
import au.com.ourpillstalkpaid.dialogs.ConfirmDeletePillDialog;
import au.com.ourpillstalkpaid.model.ScannedPill;

import static au.com.ourpillstalkpaid.util.PreferencesUtils.setPreference;

public class MedicineActivity extends AppCompatActivity implements ScannedPillAdapter.ActionListener {

    private static final int REQUEST_SCAN_DETAILS = 1;
    private ActivityMedicineBinding binding;

    private List<ScannedPill> scannedPillsFull;
    private List<ScannedPill> scannedPills = new ArrayList<>();
    private List<String> emailDatas;

    private SharedPreferences.Editor edt;
    private SharedPreferences sp;
    private ScannedPillAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_medicine);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        edt = sp.edit();

        binding.edtEditMail.setText(MyApplication.emailAddress.toString());

        binding.imgbtnDone.setVisibility(View.GONE);

        binding.imgbtnDone.setOnClickListener(v -> {
            if (UserInfoActivity.isEmailValid(binding.edtEditMail.getText().toString().trim())) {
                binding.btSendMail.setVisibility(View.VISIBLE);
                binding.txtEmail.setVisibility(View.VISIBLE);
                binding.imgbtnDone.setVisibility(View.GONE);
                binding.edtEditMail.setVisibility(View.GONE);
                binding.txtEmail.setText("mail to: " + binding.edtEditMail.getText().toString());
                setPreference("email", binding.edtEditMail.getText().toString(), MedicineActivity.this);
                edt.putBoolean("SaveInfo", true);
                edt.commit();

                MyApplication.emailAddress = binding.edtEditMail.getText().toString();
            } else {
                Toast.makeText(MedicineActivity.this, "Email Id is not valid", Toast.LENGTH_SHORT).show();
            }
        });

        binding.edtSearchAtToolbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search(binding.edtSearchAtToolbar.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (MyApplication.emailAddress.equals("")) {
            binding.txtEmail.setText("Tab here to enter Email Id");
        } else {
            binding.txtEmail.setText("mail to: " + MyApplication.emailAddress);
        }

        binding.txtEmail.setOnClickListener(v -> {
            binding.txtEmail.setVisibility(View.GONE);
            binding.btSendMail.setVisibility(View.GONE);
            binding.edtEditMail.setVisibility(View.VISIBLE);
            binding.imgbtnDone.setVisibility(View.VISIBLE);
            if (MyApplication.emailAddress.equals("")) {

            } else {
                binding.edtEditMail.setText(MyApplication.emailAddress + "");
            }
        });

        binding.searchImage.setOnClickListener(v -> {
            binding.searchImage.setVisibility(View.GONE);
            binding.crossImage.setVisibility(View.VISIBLE);
            binding.edtSearchAtToolbar.setVisibility(View.VISIBLE);
            binding.edtSearchAtToolbar.requestFocus();
        });

        binding.crossImage.setOnClickListener(v -> {
            binding.edtSearchAtToolbar.setText("");
            binding.searchImage.setVisibility(View.VISIBLE);
            binding.crossImage.setVisibility(View.GONE);
            binding.edtSearchAtToolbar.setVisibility(View.GONE);
        });

        binding.btSendMail.setOnClickListener(v -> {
            if (UserInfoActivity.isEmailValid(binding.edtEditMail.getText().toString().trim())) {
                binding.txtEmail.setVisibility(View.VISIBLE);
                binding.btSendMail.setVisibility(View.VISIBLE);
                binding.txtEmail.setText("mail to: " + binding.edtEditMail.getText().toString());
                setPreference("email", binding.edtEditMail.getText().toString(), MedicineActivity.this);
                edt.putBoolean("SaveInfo", true);
                edt.commit();

                MyApplication.emailAddress = binding.edtEditMail.getText().toString();
            } else {
                Toast.makeText(MedicineActivity.this, "Email Id is not valid", Toast.LENGTH_SHORT).show();
                return;
            }
            String recipient = binding.txtEmail.getText().toString().substring(9).trim();
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{recipient});
            i.putExtra(Intent.EXTRA_SUBJECT, "Our Pills Talk");
            i.putExtra(Intent.EXTRA_TEXT, emailDatas + "");
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
                Toast.makeText(MedicineActivity.this, "Send mail...", Toast.LENGTH_SHORT).show();
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MedicineActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
        });

        binding.btSendMail.setOnLongClickListener(v -> {
            Toast.makeText(MedicineActivity.this, "Send History", Toast.LENGTH_SHORT).show();
            return true;
        });

        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (binding.crossImage.getVisibility() == View.VISIBLE) {
            binding.searchImage.setVisibility(View.VISIBLE);
            binding.edtSearchAtToolbar.setText("");
            binding.edtSearchAtToolbar.setVisibility(View.GONE);
            binding.crossImage.setVisibility(View.GONE);
            return;
        } else if (binding.imgbtnDone.getVisibility() == View.VISIBLE) {
            binding.btSendMail.setVisibility(View.VISIBLE);
            binding.txtEmail.setVisibility(View.VISIBLE);
            binding.imgbtnDone.setVisibility(View.GONE);
            binding.edtEditMail.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SCAN_DETAILS && resultCode == RESULT_OK) {
            loadData();
        }
    }

    @Override
    public void onItemClicked(ScannedPill scannedPill) {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra("ScannedPill", scannedPill);
        startActivityForResult(intent, REQUEST_SCAN_DETAILS);
    }

    @Override
    public void onDeleteClicked(final ScannedPill scannedPill) {
        new ConfirmDeletePillDialog()
                .setScannedPill(scannedPill)
                .setCallback(() -> {
                    scannedPillsFull.remove(scannedPill);
                    scannedPills.remove(scannedPill);
                    adapter.notifyDataSetChanged();

                    DBHelper.deleteAlarmsOfPill(this, scannedPill).delete();
                }).show(getSupportFragmentManager(), null);
    }

    private void loadData() {
        scannedPillsFull = new ArrayList<>();
        List<String> xmls = new ArrayList<>();
        emailDatas = new ArrayList<>();

        List<ScannedPill> items = DBHelper.findAll();
        for (ScannedPill scannedPill : items) {
            scannedPillsFull.add(scannedPill);
            xmls.add(scannedPill.getXml());
        }

        for (int i = xmls.size() - 1; i >= 0; i--) {
            emailDatas.add(scannedPillsFull.get(i).getName() + "\n" + xmls.get(i) + "\n--------------- \n");
        }

        adapter = new ScannedPillAdapter(scannedPills, this, this);
        binding.listTotalScans.setAdapter(adapter);
        search("");
    }

    private void search(String searchText) {
        scannedPills.clear();
        for (int i = 0; i < scannedPillsFull.size(); i++) {
            if (TextUtils.isEmpty(searchText) || scannedPillsFull.get(i).getName().contains(searchText)) {
                scannedPills.add(scannedPillsFull.get(i));
            }
        }
        adapter.notifyDataSetChanged();
    }
    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activity=null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activity=this;
    }

}
