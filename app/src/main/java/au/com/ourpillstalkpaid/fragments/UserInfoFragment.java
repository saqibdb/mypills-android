package au.com.ourpillstalkpaid.fragments;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import au.com.ourpillstalkpaid.MyApplication;
import au.com.ourpillstalkpaid.R;
import au.com.ourpillstalkpaid.databinding.FragmentUserInfoBinding;

public class UserInfoFragment extends Fragment {

    private FragmentUserInfoBinding binding;
    private List<String> forUserInfo = new ArrayList<>();
    private List<String> userInfo = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_info, container, false);

        adapter = new ArrayAdapter<String>(getContext(), 0) {
            @Override
            public int getCount() {
                return userInfo.size();
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder holder;
                if (convertView == null) {
                    convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_detail, parent, false);
                    holder = new ViewHolder();
                    holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
                    holder.tvValue = (TextView) convertView.findViewById(R.id.tv_value);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }

                holder.tvTitle.setText(forUserInfo.get(position));
                holder.tvValue.setText(userInfo.get(position));
                holder.tvValue.setTypeface(null,
                        forUserInfo.get(position).equals(getString(R.string.emergency_contact_number)) ?
                                Typeface.BOLD : Typeface.NORMAL);

                return convertView;
            }
        };
        binding.listUserInfo.setAdapter(adapter);

        loadUserInfo();
        return binding.getRoot();
    }

    public void loadUserInfo() {
        userInfo.clear();
        forUserInfo.clear();

        userInfo.add(MyApplication.firstName);
        forUserInfo.add(getString(R.string.first_name));
        userInfo.add(MyApplication.lastName);
        forUserInfo.add(getString(R.string.last_name));
        userInfo.add(MyApplication.emergencyContactNumber);
        forUserInfo.add(getString(R.string.emergency_contact_number));
        userInfo.add(MyApplication.allergies);
        forUserInfo.add(getString(R.string.allergies));
        userInfo.add(MyApplication.preferedLang);
        forUserInfo.add(getString(R.string.preferred_language));
        userInfo.add(MyApplication.yearOfBirth);
        forUserInfo.add(getString(R.string.year_of_birth));
        userInfo.add(MyApplication.postCode);
        forUserInfo.add(getString(R.string.postcode));
        userInfo.add(MyApplication.gender);
        forUserInfo.add(getString(R.string.gender));
        userInfo.add(MyApplication.emailAddress);
        forUserInfo.add(getString(R.string.email_address));
        userInfo.add(MyApplication.countryOfOrigin);
        forUserInfo.add(getString(R.string.country_of_origin));
        adapter.notifyDataSetChanged();
    }

    private class ViewHolder {
        private TextView tvTitle, tvValue;
    }
}
