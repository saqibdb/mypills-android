//
// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license.
//
// Microsoft Cognitive Services (formerly Project Oxford): https://www.microsoft.com/cognitive-services
//
// Microsoft Cognitive Services (formerly Project Oxford) GitHub:
// https://github.com/Microsoft/Cognitive-Speech-TTS
//
// Copyright (c) Microsoft Corporation
// All rights reserved.
//
// MIT License:
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED ""AS IS"", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
package com.microsoft.speech.tts;

import java.util.HashMap;
import java.util.Map;

public class Voice {
    public enum Gender {
        Male, Female
    }

    public Voice(String lang) {
        this.lang = lang;
        this.voiceName = "";
        this.gender = Gender.Female;
        this.isServiceVoice = true;
    }

    public Voice(String lang, String voiceName, Gender gender, Boolean isServiceVoice) {
        this.lang = lang;
        this.voiceName = voiceName;
        this.gender = gender;
        this.isServiceVoice = isServiceVoice;
    }

    public final String lang;
    public final String voiceName;
    public final Gender gender;
    public final Boolean isServiceVoice;

    public static final Map<String, Voice> VOICES;

    static {
        VOICES = new HashMap<>();
        VOICES.put("ar", new Voice("ar-EG", "Microsoft Server Speech Text to Speech Voice (ar-EG, Hoda)", Gender.Female, true));
        VOICES.put("bg", new Voice("bg-BG", "Microsoft Server Speech Text to Speech Voice (bg-BG, Ivan)", Gender.Male, true));
        VOICES.put("ca", new Voice("ca-ES", "Microsoft Server Speech Text to Speech Voice (ca-ES, HerenaRUS)", Gender.Female, true));
        VOICES.put("cs", new Voice("cs-CZ", "Microsoft Server Speech Text to Speech Voice (cs-CZ, Jakub)", Gender.Male, true));
        VOICES.put("da", new Voice("da-DK", "Microsoft Server Speech Text to Speech Voice (da-DK, HelleRUS)", Gender.Female, true));
        VOICES.put("de", new Voice("de-DE", "Microsoft Server Speech Text to Speech Voice (de-DE, Hedda)", Gender.Female, true));
        VOICES.put("el", new Voice("el-GR", "Microsoft Server Speech Text to Speech Voice (el-GR, Stefanos)", Gender.Male, true));
        VOICES.put("en", new Voice("en-US", "Microsoft Server Speech Text to Speech Voice (en-US, ZiraRUS)", Gender.Female, true));
        VOICES.put("es", new Voice("es-ES", "Microsoft Server Speech Text to Speech Voice (es-ES, Laura, Apollo)", Gender.Female, true));
        VOICES.put("fi", new Voice("fi-FI", "Microsoft Server Speech Text to Speech Voice (fi-FI, HeidiRUS)", Gender.Female, true));
        VOICES.put("fr", new Voice("fr-FR", "Microsoft Server Speech Text to Speech Voice (fr-FR, Julie, Apollo)", Gender.Female, true));
        VOICES.put("hi", new Voice("hi-IN", "Microsoft Server Speech Text to Speech Voice (hi-IN, Kalpana, Apollo)", Gender.Female, true));
        VOICES.put("hr", new Voice("hr-HR", "Microsoft Server Speech Text to Speech Voice (hr-HR, Matej)", Gender.Male, true));
        VOICES.put("hu", new Voice("hu-HU", "Microsoft Server Speech Text to Speech Voice (hu-HU, Szabolcs)", Gender.Male, true));
        VOICES.put("id", new Voice("id-ID", "Microsoft Server Speech Text to Speech Voice (id-ID, Andika)", Gender.Male, true));
        VOICES.put("it", new Voice("it-IT", "Microsoft Server Speech Text to Speech Voice (it-IT, Cosimo, Apollo)", Gender.Male, true));
        VOICES.put("ja", new Voice("ja-JP", "Microsoft Server Speech Text to Speech Voice (ja-JP, Ayumi, Apollo)", Gender.Female, true));
        VOICES.put("ko", new Voice("ko-KR", "Microsoft Server Speech Text to Speech Voice (ko-KR, HeamiRUS)", Gender.Female, true));
        VOICES.put("ms", new Voice("ms-MY", "Microsoft Server Speech Text to Speech Voice (ms-MY, Rizwan)", Gender.Male, true));
        VOICES.put("nl", new Voice("nl-NL", "Microsoft Server Speech Text to Speech Voice (nl-NL, HannaRUS)", Gender.Female, true));
        VOICES.put("no", new Voice("nb-NO", "Microsoft Server Speech Text to Speech Voice (nb-NO, HuldaRUS)", Gender.Female, true));
        VOICES.put("pl", new Voice("pl-PL", "Microsoft Server Speech Text to Speech Voice (pl-PL, PaulinaRUS)", Gender.Female, true));
        VOICES.put("pt", new Voice("pt-PT", "Microsoft Server Speech Text to Speech Voice (pt-PT, HeliaRUS)", Gender.Female, true));
        VOICES.put("ro", new Voice("ro-RO", "Microsoft Server Speech Text to Speech Voice (ro-RO, Andrei)", Gender.Male, true));
        VOICES.put("ru", new Voice("ru-RU", "Microsoft Server Speech Text to Speech Voice (ru-RU, Irina, Apollo)", Gender.Female, true));
        VOICES.put("sk", new Voice("sk-SK", "Microsoft Server Speech Text to Speech Voice (sk-SK, Filip)", Gender.Male, true));
        VOICES.put("sl", new Voice("sl-SI", "Microsoft Server Speech Text to Speech Voice (sl-SI, Lado)", Gender.Male, true));
        VOICES.put("sv", new Voice("sv-SE", "Microsoft Server Speech Text to Speech Voice (sv-SE, HedvigRUS)", Gender.Female, true));
        VOICES.put("ta", new Voice("ta-IN", "Microsoft Server Speech Text to Speech Voice (ta-IN, Valluvar)", Gender.Male, true));
        VOICES.put("th", new Voice("th-TH", "Microsoft Server Speech Text to Speech Voice (th-TH, Pattara)", Gender.Male, true));
        VOICES.put("tr", new Voice("tr-TR", "Microsoft Server Speech Text to Speech Voice (tr-TR, SedaRUS)", Gender.Female, true));
        VOICES.put("vi", new Voice("vi-VN", "Microsoft Server Speech Text to Speech Voice (vi-VN, An)", Gender.Male, true));
        VOICES.put("zh-CN", new Voice("zh-CN", "Microsoft Server Speech Text to Speech Voice (zh-CN, HuihuiRUS)", Gender.Female, true));
        VOICES.put("zh-TW", new Voice("zh-TW", "Microsoft Server Speech Text to Speech Voice (zh-TW, Yating, Apollo)", Gender.Female, true));
    }
}
